# 2019-12 UPC++ Tutorial at NERSC

This directory contains various materials from the UPC++ tutorial
presented at [NERSC](https://nersc.gov) and sponsored by the 
[Exascale Computing Project](https://www.exascaleproject.org).

The [tutorial slides are here](https://bitbucket.org/berkeleylab/upcxx-extras/raw/master/tutorials/2019-12/tutorial_slides.pdf).

You can [download this entire repo](https://bitbucket.org/berkeleylab/upcxx-extras/get/master.zip)
or clone with a command like:
```
git clone https://bitbucket.org/berkeleylab/upcxx-extras.git
```
in either case, these tutorial materials are located in the tutorials/2019-12 directory.

Except where otherwise noted, all files in this directory are 
authored by the LBNL Pagoda group, and subject to the copyright notice 
and licensing terms in the top-level [LICENSE.txt](../../LICENSE.txt).

The UPC++ example codes have been tested with  UPC++ 1.0 v2019.9.0
which is available [here](https://upcxx.lbl.gov).

The [upcxx.lbl.gov](https://upcxx.lbl.gov) site also includes links to API
documentation, downloads and publications.

## Usage instructions for NERSC's Cori

This set of instructions are tailored to this tutorial, and some portions are
not guaranteed to work outside of this event.  For instance, the `$UPCXX_TUTORIAL`
environment variable and `tutorial-salloc` alias are provided to simplify some
details that are orthogonal to teaching use of UPC++.

**MOST IMPORTANTLY** in this tutorial one will run simple UPC++ programs on
the login nodes of Cori.  This is generally *not* an acceptable practice for real
applications, which must only be run on cori's dedicated compute nodes 
(as per NERSC computing policies).

For guidance using UPC++ on Cori outside of this tutorial, please see
[Site-specific instructions](#site-specific-instructions), below.

### 1. Connect to Cori

Participants issued a NERSC training account for this event should login to
Cori as follows, replacing `trainN` with the user id you received in advance:
```
ssh trainN@cori.nersc.gov
```
Participants using their own NERSC account should connect normally.

### 2. Setup your environment for the tutorial

The following command sets up your environment as needed for this tutorial.  
The output is shown below and includes some helpful hints that are described
in more detail later.
```
elvis@cori07:~> module load upcxx-tutorial

######################################################################
#     Welcome to the ECP/NERSC December 2019 UPC++ Tutorial          #
#                                                                    #
# * The upcxx and upcxx-run utilities are now in your $PATH          #
#                                                                    #
# * Please make a copy the tutorial materials:                       #
#      cp -r $UPCXX_TUTORIAL $HOME/                                  #
#   This will yield '2019-12' in your home directory.                #
#                                                                    #
# * Materials are also viewable online:                              #
#      https://upcxx.lbl.gov/tutorial-2019-12                        #
#                                                                    #
# * To redisplay this message at any time:                           #
#      module help upcxx-tutorial                                    #
######################################################################
## NOTICE: loaded 'upcxx-login/2019.9.0' targeting Cori login nodes
## NOTICE: `module swap upcxx-login upcxx` to target Cori compute nodes
```

### 3. Copy the tutorial materials

You should copy the tutorial materials from a local filesystem on Cori,
eliminating the need to fetch the tutorial materials from bitbucket.org:
```
elvis@cori07:~> cp -r $UPCXX_TUTORIAL $HOME/
```
This will yield a directory `2019-12` in your home directory.

### 4. Use of the tutorial materials

Having followed the steps above, you will have `upcxx` and `upcxx-run` in your
`$PATH`.  You can build and run the examples and exercises following the
[Generic usage instructions](#generic-usage-instructions), below.

### 5. Advanced use of Cori (compute nodes)

For the purpose of this tutorial, most work will be performed on the login
nodes.  However, if time allows, you are welcome to try out UPC++ on Cori's
compute nodes.

In general, executables for the login and compute nodes are not
interchangeable.  So, it is recommended to `make clean` in the examples or
exercises directories when switching between these targets.

The following commands switch the UPC++ tool chain from one targeting
shared-memory communication on the login nodes, to one targeting the Cray
Aries network on Cori's compute nodes:
```
module swap upcxx-login upcxx
```
One can switch back by reversing the arguments:
```
module swap upcxx upcxx-login
```

We have provided a `tutorial-salloc` alias to simplify access to Cori compute
nodes.  For users of NERSC training accounts this will provide access to a
pair of compute nodes from a pool reserved for this activity.  For other users
this alias allocates from Cori's standard `interactive` QOS.

To allocate two nodes for 120 minutes of interactive use (with example output):
```
elvis@cori07:~> tutorial-salloc -t120
salloc: Granted job allocation 26400438
salloc: Waiting for resource configuration
salloc: Nodes nid000[41,75] are ready for job
elvis@nid00041:~>
```

Note: The 120-minute time limit in this example is not a specific requirement.
However, if using a NERSC training account be advised that the associated
compute node reservation ends at 2:30pm PST today, and any `tutorial-salloc`
request which would extend beyond that time will be denied.

One can run the compilers on either the login nodes or compute nodes, and
`$HOME` is shared across both.  However, due to system tuning, the speed of
compilers (and responsiveness of editors) on compute nodes is noticeable
slower than on the login nodes.

### 6. Use of Cori outside of this tutorial

If you are a NERSC user and want to use UPC++ outside of this tutorial, you
will find that a SLURM batch job for a UPC++ job is essentially the same as for
an MPI job.  While `upcxx-run` provides a system-independent wrapper (like
`mpirun` on many systems), you can also use SLURM's `srun` directly on Cori,
just as you would for an MPI job.

## Generic usage instructions

Assuming you have UPC++ installed in your `$PATH`, you can 
build and run code in the `examples` or `exercises` directory with a command like the following:
```
#!sh
make all run
```
A single example or exercise can be compiled and run with a `run-[PROG]` convenience target:
```
#!sh
make run-hello-world
```
If UPC++ is installed elsewhere (e.g. `/usr/local/upcxx`) you can 
use a command like the following:
```
#!sh
make all run UPCXX_INSTALL=/usr/local/upcxx
```
If you want to adjust compile flags, you can use a command like:
```
#!sh
make clean all EXTRA_FLAGS='-O -network=udp'
```

## Getting help

During the hands-on portions of the tutorial, the instructor and several
assistants will be available to help.  Online participants, please use Zoom's
Chat function to request help and you will be paired with an assistant as one
becomes available.  Since Zoom Chat is not well suited to large volumes of
text, we suggest use of (free) [pastebin.com](pastebin.com) or similar if you
need to share compiler or runtime error messages.

## Site-specific instructions

Information about installations of UPC++ at NERSC and OLCF is available
[here](https://bitbucket.org/berkeleylab/upcxx/wiki/docs/site-docs)

One will also need to follow the download or clone instructions, above, to
obtain the tutorial materials from bitbucket.org and then use the
[Generic usage instructions](#generic-usage-instructions).

Keep in mind that execution (`make run` or `upcxx-run`) must take place within
a resource allocation (interactive or batch).  Instructions for use of the
batch schedulers at various centers are beyond the scope of this tutorial.
