// Tests the insert and find operations on the distributed hash table.
#include "drmap.hpp" // this header contains the DHT operations
#include <iostream>
#include <random>
#include <cstdint>
#undef NDEBUG // this correctness test uses assertions for validation
#include <cassert>

using namespace std;
using namespace upcxx;

uint64_t choose_key(intrank_t rank, long idx) {
  return uint64_t(rank) << 32 | (idx & 0xFFFFFFFF);
}

int main() {
  init();
  const long N = 100;
  const long szfactor = 1000;

  if (!rank_me()) cout << "Constructing a DistrRMAMap<uint64_t,uint64_t>..." << endl;
  // a map from int64 to arrays of int64
  DistrRMAMap<uint64_t,uint64_t> drmap;

  if (!rank_me()) cout << "Inserting " << N << " key/value pairs from each of " 
                       << rank_n() << " ranks, with value sizes 8.." 
                       << 8*N*szfactor << " bytes..." << endl;
  barrier();
  future<> all_inserts = make_future();
  for (long i = 1; i <= N; i++) {
    uint64_t key = choose_key(rank_me(), i);
    size_t valsz = i*szfactor;
    uint64_t *val = new uint64_t[valsz];
    val[0] = val[valsz-1] = key; // init two elements
    // initiate the insertion
    future<bool> fut = drmap.insert(key, val, valsz);
    // add to future chain:
    all_inserts = when_all(all_inserts,
      fut.then([val](bool success) { // process the result
        assert(success); // should not have collisions
        delete [] val; // free the insertion source
      })
    );
    if (!(i & 0xF)) upcxx::progress(); // occasional progress
  }
  all_inserts.wait();

  barrier(); // wait for inserts to complete globally
  if (!rank_me()) cout << "Inserts complete!\n"
                       << "Now issuing " << N << " finds from " 
                       << rank_n() << " ranks to verify..." << endl;
  barrier();

  long peer = (rank_me() + 1) % rank_n();
  future<> all_finds = make_future();
  for (long i = 1; i <= N; i++) {
    uint64_t key = choose_key(peer, i);
    size_t valsz = i*szfactor;
    // initiate the lookup
    future<uint64_t *, size_t> fut = drmap.find(key);
    // add to future chain:
    all_finds = when_all(all_finds,
      fut.then([=](uint64_t *val, size_t count) { // process the result
        // check the validity of retrieved data:
        assert(val); assert(count == valsz);
        assert(val[0] == key); assert(val[valsz-1] == key);
        delete [] val;
      })
    );
    if (!(i & 0xF)) upcxx::progress(); // occasional progress
  }
  all_finds.wait();

  barrier(); // wait for finds to complete globally
  if (!rank_me()) cout << "Find complete!" << endl;

  // optionally check corner-case behaviors
  #if INSERT_CHECKS
    if (!rank_me()) {
      cout << "Testing insert collision..." << endl;
      uint64_t key = choose_key(0, 1);
      uint64_t val[1];
      future<bool> fut = drmap.insert(key, val, 1);
      assert(!fut.wait() && "Insert collision check failed!");
    } 
  #endif
  #if FIND_CHECKS
    if (!rank_me()) {
      cout << "Testing find absent key..." << endl;
      uint64_t key = choose_key(0, N+1);
      future<uint64_t *, size_t> fut = drmap.find(key);
      assert(!fut.wait<0>() && "Find absent key check failed!");
    } 
  #endif

  if (!rank_me()) cout << "SUCCESS" << endl;
  finalize();
}
