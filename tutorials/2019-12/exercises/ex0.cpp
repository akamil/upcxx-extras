// First hello world example.
// Each process independently says hello, with no synchronization.

#include <iostream>
#include <upcxx/upcxx.hpp>

int main() {
  // setup UPC++ runtime
  upcxx::init();
  std::cout << "Hello world from process " << upcxx::rank_me()
            << " out of " << upcxx::rank_n() << " processes" << std::endl;
  // close down UPC++ runtime
  upcxx::finalize();
} 
