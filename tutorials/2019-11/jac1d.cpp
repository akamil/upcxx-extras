// Simple 1-d Jacobi solver tutorial example
// Dan Bonachea and Amir Kamil
// NOTE: This code is written in naive/simple ways to demonstrate 
// various library features for pedagogical purposes in tutorials. 
// This is NOT intended to be a high-performance solver.

#include <upcxx/upcxx.hpp>

#include <iostream>
#include <tuple>
#include <random>
#include <cstdlib>
#include <cassert>
#include <cmath>

using namespace upcxx;

// job parameters
long global_domain_sz = 1024; // total domain size
long iters = 100; // iterations to compute

// main local array data structure
global_ptr<double> old_grid_gptr, new_grid_gptr;
double *old_grid, *new_grid;

// other per-process metadata
long grid_size; // per-process array size
long local_domain_sz; 
intrank_t left, right; // ranks of my neighbors
// direct pointers to neighbor grids
global_ptr<double> left_old_grid, left_new_grid;
global_ptr<double> right_old_grid, right_new_grid;

// ---------------------------------------------------------------------------
// Utilities

// bootstrap left gptrs using rpc and futures
future<> bootstrap_left() {
  future<global_ptr<double>, global_ptr<double>> fut =
      rpc(left, []() {
        return make_future(old_grid_gptr, new_grid_gptr);
      });
  return fut.then(
    [](global_ptr<double> ptr1, global_ptr<double> ptr2) {
      left_old_grid = ptr1;
      left_new_grid = ptr2;
    });
}

// bootstrap right gptrs using dist_object
void bootstrap_right() {
  dist_object<std::tuple<global_ptr<double>,global_ptr<double>>> dobj({old_grid_gptr, new_grid_gptr});

  std::tie(right_old_grid, right_new_grid) = dobj.fetch(right).wait();

  upcxx::barrier(); // ensures dobj is not destructed until all ranks have it
}

// permanent setup: establish main datastructures and metadata
void setup() {
  // allocate grid in shared space
  old_grid_gptr = new_array<double>(grid_size);
  new_grid_gptr = new_array<double>(grid_size);

  // downcast to raw C++ pointers to our grid
  old_grid = old_grid_gptr.local();
  new_grid = new_grid_gptr.local();

  left = (rank_me() + rank_n() - 1) % rank_n();
  right = (rank_me() + 1) % rank_n();

  upcxx::barrier(); // ensure local metadata established

  // exchange remote info needed by some algorithms
  bootstrap_left().wait();
  bootstrap_right();

  upcxx::barrier();
}


// check invariants
void sanity_check() {
  upcxx::barrier();
  assert(old_grid_gptr && new_grid_gptr);
  assert(old_grid && new_grid);
  assert(old_grid == old_grid_gptr.local());
  assert(new_grid == new_grid_gptr.local());
  assert((left + 1) % rank_n() == rank_me());
  assert((rank_me() + 1) % rank_n() == right);
  assert(left_old_grid && left_new_grid);
  assert(left_old_grid.where() == left && left_new_grid.where() == left);
  assert(right_old_grid && right_new_grid);
  assert(right_old_grid.where() == right && right_new_grid.where() == right);
  upcxx::barrier();
}

// setup grid for each test
void init_grid() {
  // init to uniform pseudo-random distribution, independent of job size
  long baseidx = rank_me() * local_domain_sz;
  std::mt19937_64 rgen(1);  rgen.discard(baseidx);
  for (long i = 1; i < grid_size - 1; ++i) {
    old_grid[i] = rgen() % 100;
  }
  // clear ghost cells and other grid points
  old_grid[0] = std::nan("1");
  old_grid[grid_size - 1] = std::nan("2");
  for (long i = 0; i < grid_size; ++i) {
    new_grid[i] = std::nan("3");
  }
}

// check for correct answer
bool check_grid() {
  double sum = 0;
  for (long i = 1; i < grid_size - 1; ++i) {
    sum += old_grid[i]; 
  }
  sum = reduce_all(sum, op_fast_add).wait();

  // check known results for global_domain_sz x iters
  static struct { long sz, iters; double correct; } known_answers[] = {
                {    8192,   100,       408097 },
                {    4096,   100,       207466 },
                {    2048,   100,       103965 },
                {    1024,   100,        51900 },
                {     512,   100,        26021 },
                {     256,   100,        12596 },
                {     128,   100,         6129 },
                {      64,   100,         2943 },
                {      32,   100,         1502 },
                {      16,   100,          760 },
                {       8,   100,          352 },
                {       4,   100,          166 },
  };

  const char *result = "(unknown problem size)";
  bool success = true;
  for (auto &answer : known_answers) {
    if (global_domain_sz == answer.sz
        /* && iters == answer.iters */ // iteration count does not mathematically affect sum
        ) {
      if (fabs(sum - answer.correct) < 0.001) 
           result = "PASSED";
      else { result = "FAILED"; success = false; }
      break;
    }
  }

  if (!rank_me()) std::cout << "  result = " << sum << "  " << result << std::endl;
  return success;
}

double get_cell(long i) {
  return old_grid[i];
}

// ---------------------------------------------------------------------------
// Simple RMA get version
// this version communicates with RMA get and synchronizes with barrier
void jacobi_rget() {
  for (long it = 0; it < iters; it++) {
    future<double> left_ghost  = rget(left_old_grid+grid_size-2);
    future<double> right_ghost = rget(right_old_grid+1);

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    // consume fetched values to compute boundary
    new_grid[1] = 0.25 *
      (left_ghost.wait() + 2*old_grid[1] + old_grid[2]);
    new_grid[grid_size-2] = 0.25 *
      (old_grid[grid_size-3] + 2*old_grid[grid_size-2] + right_ghost.wait());

    upcxx::barrier(); // ensure incoming rgets serviced before next iter overwrites

    // setup next iteration
    std::swap(old_grid, new_grid);
    std::swap(left_old_grid, left_new_grid);
    std::swap(right_old_grid, right_new_grid);
  } // iteration loop
}
// ---------------------------------------------------------------------------
// Simple RMA put version
// this version communicates with RMA put and synchronizes with barrier
void jacobi_rput() {
  for (long it = 0; it < iters; it++) {
    // push to neighboring ghost cells
    future<> puts = when_all(
                      rput(old_grid[1], left_old_grid+grid_size-1),
                      rput(old_grid[grid_size-2], right_old_grid)  );

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    puts.wait();
    upcxx::barrier(); // ensure incoming puts have arrived

    // consume arrived values to compute boundary
    new_grid[1] = 0.25 *
      (old_grid[0] + 2*old_grid[1] + old_grid[2]);
    new_grid[grid_size-2] = 0.25 *
      (old_grid[grid_size-3] + 2*old_grid[grid_size-2] + old_grid[grid_size-1]);

    // setup next iteration
    std::swap(old_grid, new_grid);
    std::swap(left_old_grid, left_new_grid);
    std::swap(right_old_grid, right_new_grid);
  } // iteration loop
}
// ---------------------------------------------------------------------------
// Simple rpc version
// this version communicates with rpc and synchronizes with barrier
void jacobi_rpc() {
  for (long it = 0; it < iters; it++) {
    future<double> left_ghost  = rpc(left,  get_cell, grid_size-2);
    future<double> right_ghost = rpc(right, get_cell, 1);

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    // consume fetched values to compute boundary
    new_grid[1] = 0.25 *
      (left_ghost.wait() + 2*old_grid[1] + old_grid[2]);
    new_grid[grid_size-2] = 0.25 *
      (old_grid[grid_size-3] + 2*old_grid[grid_size-2] + right_ghost.wait());

    upcxx::barrier(); // ensure all incoming rpcs are processed
    std::swap(old_grid, new_grid);
    upcxx::barrier(); // ensure swap is done before new rpcs can arrive
  } // iteration loop
}

// ---------------------------------------------------------------------------
// rpc with callbacks version
// this version fetches ghost values via rpc and computes the boundary in callback
void jacobi_rpc_callbacks() {
  for (long it = 0; it < iters; it++) {
    future<> left_update = rpc(left, get_cell, grid_size-2)
      .then([](double value) {
        new_grid[1] = 0.25 *
          (value + 2*old_grid[1] + old_grid[2]);
      });
    future<> right_update = rpc(right, get_cell, 1)
      .then([](double value) {
        new_grid[grid_size-2] = 0.25 *
          (old_grid[grid_size-3] + 2*old_grid[grid_size-2] + value);
      });

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    // await completion of boundary cells
    left_update.wait();
    right_update.wait();

    upcxx::barrier(); // ensure all incoming rpcs are processed
    std::swap(old_grid, new_grid);
    upcxx::barrier(); // ensure swap is done before new rpcs can arrive
  } // iteration loop
}

// ---------------------------------------------------------------------------
// rpc with chaining and conjoining
// this version fetches ghost values via rpc, chains and conjoins boundary computation
void jacobi_rpc_conjoin() {
  for (long it = 0; it < iters; it++) {
    future<> all_update = when_all(
      rpc(left, get_cell, grid_size-2)
        .then([](double value) {
          new_grid[1] = 0.25 *
            (value + 2*old_grid[1] + old_grid[2]);
        }),
      rpc(right, get_cell, 1)
        .then([](double value) {
        new_grid[grid_size-2] = 0.25 *
          (old_grid[grid_size-3] + 2*old_grid[grid_size-2] + value);
        })
    );

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    // await completion of boundary cells
    all_update.wait();

    upcxx::barrier(); // ensure all incoming rpcs are processed
    std::swap(old_grid, new_grid);
    upcxx::barrier(); // ensure swap is done before new rpcs can arrive
  } // iteration loop
}
// ---------------------------------------------------------------------------
// Phased rpc version
// this is a variant of jacobi_rpc() that removes one barrier
void jacobi_rpc_phased() {
  static double *grids[2] = { old_grid, new_grid };
  upcxx::barrier(); // ensure static init
  auto get_phased_cell = [](long idx, int phase) { return grids[phase][idx]; };

  int phase = 0;
  for (long it = 0; it < iters; it++) {
    future<double> left_ghost =  rpc(left,  get_phased_cell, grid_size-2, phase);
    future<double> right_ghost = rpc(right, get_phased_cell, 1, phase);
    double *curr_old = grids[phase];
    double *curr_new = grids[!phase];

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      curr_new[i] = 0.25 * (curr_old[i-1] + 2*curr_old[i] + curr_old[i+1]);

    // consume fetched values to compute boundary
    curr_new[1] = 0.25 *
      (left_ghost.wait() + 2*curr_old[1] + curr_old[2]);
    curr_new[grid_size-2] = 0.25 *
      (curr_old[grid_size-3] + 2*curr_old[grid_size-2] + right_ghost.wait());

    upcxx::barrier(); // ensure boundary updates are written before next iteration
    phase = !phase;
  } // iteration loop
}
// ---------------------------------------------------------------------------
// Phased rpc pull version
// this is a pull variant of jacobi_rpc() synchronized p2p without barriers
void jacobi_rpc_pull_p2p() {
  static promise<double> boundary[2][2];
  for (auto &pp : boundary) for (auto &p : pp) p.require_anonymous(1);
  upcxx::barrier(); // ensure static init
  auto get_boundary = [](int phase, int isleft) { return boundary[phase][isleft].finalize(); };

  int phase = 0;
  for (long it = 0; it < iters; it++) {
    // post my boundary
    boundary[phase][1].fulfill_result(old_grid[1]);
    boundary[phase][0].fulfill_result(old_grid[grid_size-2]);

    // request neighbor boundary
    future<double> left_ghost =  rpc(left,  get_boundary, phase, 0);
    future<double> right_ghost = rpc(right, get_boundary, phase, 1);

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    // consume fetched values to compute boundary
    new_grid[1] = 0.25 *
      (left_ghost.wait() + 2*old_grid[1] + old_grid[2]);
    new_grid[grid_size-2] = 0.25 *
      (old_grid[grid_size-3] + 2*old_grid[grid_size-2] + right_ghost.wait());

    // setup next iteration
    for (auto &p : boundary[phase]) {
      p.get_future().wait(); // ensure neighbor has fetched
      p = promise<double>();
      p.require_anonymous(1);
    }

    std::swap(old_grid, new_grid);
    phase = !phase;
  } // iteration loop
}
// ---------------------------------------------------------------------------
// Phased rpc push version
// this is a push variant of jacobi_rpc() synchronized p2p without barriers
void jacobi_rpc_push_p2p() {
  static promise<double> boundary[2][2];
  upcxx::barrier(); // ensure static init
  auto push_boundary = [](int phase, int isleft, double val) { boundary[phase][isleft].fulfill_result(val); };

  int phase = 0;
  for (long it = 0; it < iters; it++) {
    // push my boundary
    rpc_ff(left,  push_boundary, phase, 1, old_grid[1]);
    rpc_ff(right, push_boundary, phase, 0, old_grid[grid_size-2]);

    // compute interior cells
    for (long i = 2; i < grid_size - 2; ++i)
      new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

    // consume arrived neighbor values to compute boundary
    new_grid[1] = 0.25 *
      (boundary[phase][0].get_future().wait() + 2*old_grid[1] + old_grid[2]);
    new_grid[grid_size-2] = 0.25 *
      (old_grid[grid_size-3] + 2*old_grid[grid_size-2] + boundary[phase][1].get_future().wait());

    // setup next iteration
    boundary[phase][0] = promise<double>();
    boundary[phase][1] = promise<double>();

    std::swap(old_grid, new_grid);
    phase = !phase;
  } // iteration loop
}
// ---------------------------------------------------------------------------
// test driver
// Usage: upcxx-run -np PROCS jac1d [iters] [global_domain_sz]
int main(int argc, char *argv[]) {
  upcxx::init();
  
  if (argc > 1) iters = atol(argv[1]);
  if (argc > 2) global_domain_sz = atol(argv[2]);

  if (!rank_me()) std::cout << "Running 1-d Jacobi on " << rank_n() <<  " procs, iters=" << iters 
                            << " global_domain_sz=" << global_domain_sz << std::endl;

  local_domain_sz = global_domain_sz / rank_n(); 
  grid_size = local_domain_sz + 2; // 2 ghost cells per process

  if (!rank_me()) { // check job parameters
    if (iters % 2) {
      std::cerr << "iters must be even" << std::endl;
      abort();
    }
    if (global_domain_sz != local_domain_sz * rank_n()) {
      std::cerr << "Num ranks must evenly divide global_domain_sz" << std::endl;
      abort();
    }
    if (local_domain_sz < 3) {
      std::cerr << "global_domain_sz must be provide at least 3 grid points per process" << std::endl;
      abort();
    }
  }

  upcxx::barrier();

  setup(); // init permanent data structures

  // main test driver
  struct { void (*fp)(void); const char *desc; } variants[] = {
         { jacobi_rget,          "jacobi_rget" },
         { jacobi_rput,          "jacobi_rput" },
         { jacobi_rpc,           "jacobi_rpc" },
         { jacobi_rpc_callbacks, "jacobi_rpc_callbacks" },
         { jacobi_rpc_conjoin,   "jacobi_rpc_conjoin" },
         { jacobi_rpc_phased,    "jacobi_rpc_phased" },
         { jacobi_rpc_push_p2p,  "jacobi_rpc_push_p2p" },
         { jacobi_rpc_pull_p2p,  "jacobi_rpc_pull_p2p" },
  };
  bool success = true;
  for (auto & v : variants) {
    upcxx::barrier();
    sanity_check();
    init_grid();
    if (!rank_me()) std::cout << "Running " << v.desc << "..." << std::endl;
    upcxx::barrier();
    v.fp();
    success &= check_grid();
    upcxx::barrier();
  }

  sanity_check();

  if (!rank_me()) std::cout << (success?"SUCCESS":"FAILED") << std::endl;
  upcxx::finalize();
}

