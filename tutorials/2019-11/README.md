# 2019-11 UPC++ Tutorial at NERSC #

This directory contains various materials from the UPC++ tutorial
presented at [NERSC](https://nersc.gov) and sponsored by the 
[Exascale Computing Program](https://www.exascaleproject.org).

The [tutorial slides are here](https://bitbucket.org/berkeleylab/upcxx-extras/raw/master/tutorials/2019-11/tutorial_slides.pdf).

You can [download this entire repo](https://bitbucket.org/berkeleylab/upcxx-extras/get/master.zip)
or clone with a command like:
```
git clone https://bitbucket.org/berkeleylab/upcxx-extras.git
```
in either case, these tutorial materials are located in the tutorials/2019-11 directory.

Except where otherwise noted, all files in this directory are 
authored by the LBNL Pagoda group, and subject to the copyright notice 
and licensing terms in the top-level [LICENSE.txt](../../LICENSE.txt).

The UPC++ example codes have been tested with  UPC++ 1.0 v2019.9.0
which is available [here](https://upcxx.lbl.gov).

## Usage instructions

Assuming you have UPC++ installed in your `$PATH`, you can 
build and run the examples with a command like the following:
```
#!sh
make all run
```
If UPC++ is installed elsewhere (e.g. `/usr/local/upcxx`) you can 
use a command like the following:
```
#!sh
make all run UPCXX_INSTALL=/usr/local/upcxx
```
If you want to adjust compile flags, you can use a command like:
```
#!sh
make clean all EXTRA_FLAGS='-O -network=udp'
```

## Site-specific instructions

Information about installations of UPC++ at NERSC and OLCF is available
[here](https://bitbucket.org/berkeleylab/upcxx/wiki/docs/site-docs)
