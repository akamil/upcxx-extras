#include <iostream>
#include <fstream>
#include <string>
#include "upcxx-extras/dist_array.hpp"

using namespace upcxx;

int main(int argc, char *argv[]) {
    std::string filename = "examples/input";
    if (argc>1) {
        filename = argv[1];
        if (argc>2) {
            std::cerr << "Usage: upcxx-run -n [RANKS] ./DA-fileinput [file]" << std::endl;
            std::terminate();
        }
    }
    std::ifstream input_file;
    size_t N;
    init();
    if (!local_team().rank_me()) {
        input_file.open(filename, std::ifstream::in);
        input_file >> N;
    }
    N = broadcast(N, 0, local_team()).wait();
    // alloc array over local team, arbitrary cyclic blocking
    extras::dist_array<uint64_t> my_input_data(N, 1, local_team());  
    if (!local_team().rank_me()) {
        uint64_t sum = 0;
        for (future<global_ptr<uint64_t>> pelem : my_input_data) { // uses element iterators
            uint64_t *ptr = pelem.wait().local();
            input_file >> *ptr;  // populate array elements
            sum += *ptr;
        }
      // scan through the dist_array to verify correct assignment
        for (future<global_ptr<uint64_t>> pelem : my_input_data) // uses element iterators
            sum -= *(pelem.wait().local());
        upcxx_assert_msg(!sum, "sum of file elements doesn't match sum of dist_array");
    }
    barrier();
    if (!rank_me()) std::cout << "SUCCESS" << std::endl;
    finalize();
}
