#ifndef _dist_array_h
#define _dist_array_h

#include <upcxx/upcxx.hpp>
#include <algorithm>
#include <list>
#include <map>
#include <random>
#include <mutex>

#if UPCXX_ASSERT_ENABLED
#define upcxx_assert_msg(exp, msg) \
  do { \
    if (!(exp)) { \
    std::cerr << "Assertion `" #exp "` failed in " << __FILE__ \
              << " line " << __LINE__ << ": " << msg << std::endl; \
    std::terminate(); \
    } \
  } while(0)
#else
#define upcxx_assert_msg(exp, msg) do {  } while(0)
#endif

namespace upcxx { namespace extras {

static constexpr size_t DYNAMIC_BLOCKED = (size_t)-1;
static constexpr size_t PURE_BLOCKED = (size_t)0;

template<typename T, size_t BS = DYNAMIC_BLOCKED, bool thread_safe = false>
class dist_array {

template<typename Y>
struct deref_helper : public Y {
    deref_helper(Y t) : Y(t) {}
    Y* operator->()
    { return this; }
    Y& operator*()
    { return *this; }
};

protected:
class cache;

public:
class elemiter;
class blociter;
class elemriter;
class blocriter;
class params;
class info;
class process_array;

using element_type = T;
using value_type = future<global_ptr<T>>;
using iterator = elemiter;
using block_iterator = blociter;
using reverse_iterator = elemriter;
using reverse_block_iterator = blocriter;

static params default_cache(team &t = world()) {
    class params p(t.rank_n());
    return p;
}

// all arguments to both constructors are single-valued
dist_array(size_t count)
    : dist_array(count, BS, world(), default_cache(world())) {}

dist_array(size_t count, size_t block_size)
    : dist_array(count, block_size, world(), default_cache(world())) {}

dist_array(size_t count, size_t block_size, team &t)
    : dist_array(count, block_size, t, default_cache(t)) {}

dist_array(size_t count, size_t block_size, team &t, params p)
    : N(count), m_team(t), m_cache(p)
{
    upcxx_assert_msg(BS == block_size || BS == DYNAMIC_BLOCKED 
        || block_size == DYNAMIC_BLOCKED, 
        "incompatible block sizes specified in template and constructor");
    if (BS*block_size && (BS&block_size) != DYNAMIC_BLOCKED)
        block_sz = (BS == DYNAMIC_BLOCKED ? block_size : BS);
    else
        block_sz = (count+m_team.rank_n()-1)/m_team.rank_n();
    //global blocks
    gbl_blocks = count/block_sz;
    full = gbl_blocks*block_sz;
    trail = count%block_sz;
    //local blocks and local size
    loc_blocks = gbl_blocks/t.rank_n();
    loc_size = loc_blocks*block_sz;
    if (m_team.rank_me() < (intrank_t)gbl_blocks%m_team.rank_n()) {
        loc_blocks++;
        loc_size += block_sz;
    }
    if (trail) {
        if (m_team.rank_me() == (intrank_t)gbl_blocks%m_team.rank_n()) {
            loc_blocks++;
            loc_size += trail;
        }
        gbl_blocks++;
    }
    // trailing blocks are allocated as full blocks
    A = new_array<T>(std::min(loc_blocks*block_sz, count));
    DA = new dist_object<global_ptr<T>>(A, m_team);
    if (m_cache.capacity() >= (size_t)m_team.rank_n())
        m_cache.fill(this);
}

void destroy(entry_barrier lev = entry_barrier::user) {
    upcxx_assert_msg(A&&DA, "destroy already called");
    if (lev != entry_barrier::none)
        barrier(m_team); // to ensure quiescence
    delete_array(A);
    A = nullptr;
    delete DA;
} 

~dist_array() { 
    upcxx_assert_msg(!(initialized()&&A), 
            "dist_array::destroy() was not collectively called"); 
}

//TODO: elide this if caller knows that index is in same block as last one
future<global_ptr<T>> ptr(size_t index)
{
    info coord = global_idx_info(index);
    intrank_t target = coord.rank();
    size_t offset = coord.process_idx();
    if(target == m_team.rank_me())
        return make_future(A+offset);
    // look for target in cache (locks if thread_safe)
    global_ptr<T> gptr = m_cache.find(target);
    // if found, return base pointer + offset
    if (gptr)
        return make_future(gptr + offset);
    // if not found, see if it's already being looked for
    std::lock_guard<std::mutex> lg(m_cache.tex);
    auto it = m_cache.want.find(target);
    // if it has been looked for...
    if (it != m_cache.want.end()) {
        // see if current persona is trying to find it
        for (auto elt = it->second.begin(); elt != it->second.end(); elt++) {
            // if it is, use the future from the earlier search
            if (elt->first == &upcxx::current_persona())
                return elt->second->get_future().then([offset](global_ptr<T> base)
                        { return base + offset; });
        }
        // if this is the persona's first search for the target, make promise for that
        upcxx::promise<global_ptr<T>> *p = new upcxx::promise<global_ptr<T>>;
        it->second.push_back(std::make_pair(&upcxx::current_persona(),p));
        return p->get_future().then([offset](global_ptr<T> base)
                { return base + offset; });
    // if it hasn't been looked for...
    } else {
        // update the want cache with the target, persona, and promise...
        upcxx::promise<global_ptr<T>> *p = new upcxx::promise<global_ptr<T>>;
        auto it = m_cache.want.insert({target,std::list<std::pair<upcxx::persona*,upcxx::promise<global_ptr<T>>*>>{}}).first;
        it->second.push_back(std::make_pair(&upcxx::current_persona(),p));
        // fetch the target's base pointer...
        DA->fetch(target).then([this,target,offset](global_ptr<T> base) {
            std::lock_guard<std::mutex> gl(m_cache.tex);
            // ...update cache (respecting cache size, atomicity, and duplicate entries)...
            m_cache.update(target,base);
            // ...then loop through waiting personas for this target...
            auto pend = m_cache.want.find(target)->second;
            for (auto elt : pend) {
                // ...and fulfill their promises
            upcxx::promise<global_ptr<T>> *p = elt.second;
            elt.first->lpc_ff([p,base]() {
                    p->fulfill_result(base);
                    delete p;
                });
            }
            m_cache.want.erase(target);
        });
        return p->get_future().then([offset](global_ptr<T> base) 
                { return base + offset; });
    }
}

size_t block_size() const
{ return block_sz; }

size_t process_size() const
{ return loc_size; }

size_t process_size(intrank_t rank) const { 
    upcxx_assert_msg(rank < m_team.rank_n(), "provided rank >= team's size");
    if (rank == m_team.rank_me())
        return loc_size;
    size_t ranks = m_team.rank_n(), stride = ranks*block_sz;
    size_t size = block_sz*(N/stride), mod = N%stride;
    if (rank < mod/block_sz)
        size += block_sz;
    else if (rank == mod/block_sz)
        size += mod%block_sz;
    return size;
}

size_t global_size() const
{ return N; }

size_t process_blocks() const
{ return loc_blocks; }

size_t process_blocks(intrank_t rank) const { 
    upcxx_assert_msg(rank < m_team.rank_n(), "provided rank >= team's size");
    if (rank == m_team.rank_me())
        return loc_blocks;
    size_t ranks = m_team.rank_n();
    size_t blocks = gbl_blocks/ranks, extra = gbl_blocks%ranks;
    if (rank < extra)
        blocks++;
    return blocks;
}

size_t global_blocks() const
{ return gbl_blocks; }

upcxx::team& team() { return m_team; }
const upcxx::team& team() const { return m_team; }

cache& cache() const
{ return m_cache; }

T* data() const
{ return A.local(); }

process_array process_view()
{ return process_array(this); }

elemiter begin()
{ return elemiter(this, 0); }

blociter bbegin()
{ return blociter(this, 0); }

elemiter end()
{ return elemiter(this, N); }

blociter bend()
{ return blociter(this, gbl_blocks*block_sz); }

elemriter rbegin()
{ return elemriter(this, N-1); }

blocriter brbegin()
{ return blocriter(this, (gbl_blocks-1)*block_sz); }

elemriter rend()
{ return elemriter(this, -1); }

blocriter brend()
{ return blocriter(this, -block_sz); }

info global_idx_info(size_t global_idx)
{
    upcxx_assert_msg(global_idx < N, "global index >= global size");
    size_t global_block = global_idx/block_sz;
    intrank_t rank = global_block%m_team.rank_n();
    size_t local_block = global_block/m_team.rank_n();
    size_t phase = global_idx%block_sz;
    size_t local_idx = local_block*block_sz + phase;
    return info(global_idx, global_block, local_idx, local_block, rank, phase);
}

info global_block_info(size_t global_block, bool backward = false)
{
    upcxx_assert_msg(global_block < gbl_blocks, 
            "provided global block >= global blocks");
    size_t local_block = global_block/m_team.rank_n();
    intrank_t rank = global_block%m_team.rank_n();
    size_t global_idx = global_block*block_sz;
    size_t phase = 0;
    if (backward) {
        if (global_idx+block_sz < N) {
            global_idx += block_sz-1;
            phase = block_sz-1;
        } else { // last block is only partially full
            global_idx = N-1;
            phase = global_idx % block_sz;
        }
    }
    size_t local_idx = local_block*block_sz + phase;
    return info(global_idx, global_block, local_idx, local_block, rank, phase);
}

info process_idx_info(size_t local_idx)
{ return process_idx_info(local_idx, m_team.rank_me()); }

info process_idx_info(size_t local_idx, intrank_t rank)
{
    upcxx_assert_msg(local_idx < process_size(), 
            "provided local index >= local size");
    upcxx_assert_msg(rank < m_team.rank_n(), "provided rank >= team's size");
    size_t phase = local_idx%block_sz;
    size_t local_block = local_idx/block_sz;
    size_t global_block = local_block*m_team.rank_n() + rank;
    size_t global_idx = global_block*block_sz + phase;
    return info(global_idx, global_block, local_idx, local_block, rank, phase);
}

info process_block_info(size_t local_block)
{ return process_block_info(local_block, m_team.rank_me()); }

info process_block_info(size_t local_block, intrank_t rank, bool backward = false)
{
    upcxx_assert_msg(local_block < process_blocks(), 
            "provided local block >= local blocks");
    upcxx_assert_msg(rank < m_team.rank_n(), "provided rank >= team's size");
    size_t global_block = local_block*m_team.rank_n() + rank;
    size_t global_idx = global_block*block_sz;
    size_t phase = 0;
    if (backward) {
        if (global_idx+block_sz < N) {
            global_idx += block_sz-1;
            phase = block_sz-1;
        } else { // last block is only partially full
            global_idx = N-1;
            phase = global_idx % block_sz;
        }
    }
    size_t local_idx = local_block*block_sz + phase;
    return info(global_idx, global_block, local_idx, local_block, rank, phase);
}

class elemiter {
public:
    using difference_type = ptrdiff_t;

    elemiter() : da(nullptr), idx(0) {}

    elemiter(dist_array *dist, ssize_t index) : da(dist), idx(index) {}

    elemiter(const elemiter &orig) : da(orig.da), idx(orig.idx) {}

    elemiter& operator =(const elemiter &rhs) {
        da = rhs.da;
        idx = rhs.idx;
        return *this;
    }

    elemiter& operator++() { 
        ++idx;
        return *this;
    }

    elemiter operator++(int) {
        elemiter old(*this);
        idx++;
        return old;
    }

    elemiter& operator--() { 
        --idx; 
        return *this;
    }

    elemiter operator--(int) {
        elemiter old(*this);
        idx--;
        return old;
    }

    elemiter& operator+=(const ssize_t shift) {
        idx += shift;
        return *this;
    }

    elemiter& operator-=(const ssize_t shift) {
        idx -= shift;
        return *this;
    }

    future<global_ptr<T>> operator* () const 
    { return da->ptr(idx); }
    deref_helper<future<global_ptr<T>>> operator->() const {
        future<global_ptr<T>> f = da->ptr(idx); 
        return deref_helper<future<global_ptr<T>>>(f);
    }
    future<global_ptr<T>> operator[](const ssize_t index) const 
    { return da->ptr(idx+index); }
    bool                  operator == (const elemiter &rhs) const
    { return idx == rhs.idx; }
    bool                  operator != (const elemiter &rhs) const
    { return idx != rhs.idx; }
    bool                  operator <  (const elemiter &rhs) const
    { return idx < rhs.idx; }
    bool                  operator >  (const elemiter &rhs) const
    { return idx > rhs.idx; }
    bool                  operator <= (const elemiter &rhs) const
    { return idx <= rhs.idx; }
    bool                  operator >= (const elemiter &rhs) const
    { return idx >= rhs.idx; }
    difference_type       operator -  (const elemiter &rhs) const
    { return idx - rhs.idx; }
    elemiter              operator +  (const ssize_t shift) const 
    { return elemiter(this->da, this->idx+shift); }
    elemiter              operator -  (const ssize_t shift) const 
    { return elemiter(this->da, this->idx-shift); }
    friend elemiter       operator+   (difference_type lhs, const elemiter& rhs)
    { return elemiter(lhs + rhs.idx); }
    friend elemiter       operator-   (difference_type lhs, const elemiter& rhs)
    { return elemiter(lhs - rhs.idx); }

private:
    dist_array *da;
    ssize_t idx;
};

class blociter {
public:
    using difference_type = ptrdiff_t;

    blociter() : da(nullptr), idx(0), block_sz(0) {}

    blociter(dist_array *dist, ssize_t index) 
        : da(dist), idx(index), block_sz(da->block_sz), 
          full(da->full), trail(da->trail) {
            upcxx_assert_msg(index%block_sz==0, "blociter must be constructed "
                    "with index at beginning of block");
        }

    blociter(const blociter &orig) 
        : da(orig.da), idx(orig.idx), block_sz(orig.block_sz) {}

    blociter& operator=(const blociter &rhs) {
        da = rhs.da;
        idx = rhs.idx;
        block_sz = rhs.block_sz;
        return *this;
    }

    blociter& operator++() {
        idx += block_sz;
        return *this;
    }

    blociter operator++(int) {
        blociter old(*this);
        idx += block_sz;
        return old;
    }

    blociter& operator--() {
        idx -= block_sz;
        return *this;
    }

    blociter operator--(int) {
        blociter old(*this);
        idx -= block_sz;
        return old;
    }

    blociter& operator+=(const ssize_t shift) {
        idx += block_sz*shift;
        return *this;
    }

    blociter& operator-=(const ssize_t shift) {
        idx -= block_sz*shift;
        return *this;
    }

    future<global_ptr<T>> operator* () const
    { return da->ptr(idx); }
    deref_helper<future<global_ptr<T>>> operator->() const {
        future<global_ptr<T>> f = da->ptr(idx); 
        return deref_helper<future<global_ptr<T>>>(f);
    }
    future<global_ptr<T>> operator[] (const ssize_t index) const
    { return da->ptr(idx+block_sz*index); }

    bool                  operator == (const blociter &rhs) const
    { return idx == rhs.idx; }
    bool                  operator != (const blociter &rhs) const
    { return idx != rhs.idx; }
    bool                  operator <  (const blociter &rhs) const
    { return idx <  rhs.idx; }
    bool                  operator >  (const blociter &rhs) const
    { return idx >  rhs.idx; }
    bool                  operator <= (const blociter &rhs) const
    { return idx <= rhs.idx; }
    bool                  operator >= (const blociter &rhs) const
    { return idx >= rhs.idx; }

    difference_type       operator -  (const blociter &rhs) const
    { return (idx-rhs.idx)/block_sz; }
    blociter              operator +  (const ssize_t shift) const 
    { return blociter(this->da, this->idx+block_sz*shift); }
    blociter              operator -  (const ssize_t shift) const 
    { return blociter(this->da, this->idx-block_sz*shift); }
    friend blociter       operator +  (difference_type lhs, const blociter& rhs) 
    { return blociter(lhs*rhs.block_sz + rhs.idx); }
    friend blociter       operator -  (difference_type lhs, const blociter& rhs) 
    { return blociter(lhs*rhs.block_sz - rhs.idx); }

    size_t block_size() const { 
      if ((size_t)idx < full) { // cast intentionally wraps negative values
        return block_sz;
      } else if ((size_t)idx < da->N) {
        return trail;
      } else { 
        return 0; // iterator is out-of-bounds
      }
    }

private:
    dist_array *da;
    ssize_t idx;
    size_t block_sz, full, trail;
};

class elemriter {
public:
    using difference_type = ptrdiff_t;

    elemriter() : da(nullptr), idx(0) {}

    elemriter(dist_array *dist, ssize_t index) : da(dist), idx(index) {}

    elemriter(const elemriter &rhs) : da(rhs.da), idx(rhs.idx) {}

    elemriter& operator=(const elemriter &rhs) {
        da = rhs.da;
        idx = rhs.idx;
        return *this;
    }

    elemriter&  operator++() { 
        --idx; 
        return *this;
    }

    elemriter operator++(int) {
        elemriter old(*this);
        idx--;
        return old;
    }

    elemriter& operator--() { 
        ++idx; 
        return *this;
    }

    elemriter operator--(int) {
        elemriter old(*this);
        idx++;
        return old;
    }

    elemriter& operator+=(ssize_t shift) {
        idx -= shift;
        return *this;
    }

    elemriter& operator-=(ssize_t shift) {
        idx += shift;
        return *this;
    }

    future<global_ptr<T>> operator* () const
    { return da->ptr(idx); }
    deref_helper<future<global_ptr<T>>> operator->() const {
        future<global_ptr<T>> f = da->ptr(idx); 
        return deref_helper<future<global_ptr<T>>>(f);
    }
    future<global_ptr<T>> operator[] (const ssize_t index) const
    { return da->ptr(idx-index); }
    bool                  operator == (const elemriter &rhs) const
    { return idx == rhs.idx; }
    bool                  operator != (const elemriter &rhs) const
    { return idx != rhs.idx; }
    bool                  operator <  (const elemriter &rhs) const
    { return idx <  rhs.idx; }
    bool                  operator >  (const elemriter &rhs) const
    { return idx >  rhs.idx; }
    bool                  operator <= (const elemriter &rhs) const
    { return idx <= rhs.idx; }
    bool                  operator >= (const elemriter &rhs) const
    { return idx >= rhs.idx; }
    difference_type       operator -  (const elemriter &rhs) const
    { return rhs.idx - idx; }
    elemriter             operator +  (const ssize_t shift) const 
    { return elemriter(this->da, this->idx-shift); }
    elemriter             operator -  (const ssize_t shift) const 
    { return elemriter(this->da, this->idx+shift); }
    friend elemriter       operator+  (difference_type lhs, const elemriter& rhs)
    { return elemriter(rhs.idx + lhs); }
    friend elemriter       operator-  (difference_type lhs, const elemriter& rhs)
    { return elemriter(rhs.idx - lhs); }

private:
    dist_array *da;
    ssize_t idx;
};

class blocriter {
public:
    using difference_type = ptrdiff_t;

    blocriter() : da(nullptr), idx(0), block_sz(0) {}

    blocriter(dist_array *dist, ssize_t index) 
        : da(dist), idx(index), block_sz(da->block_sz), 
          full(da->full), trail(da->trail) {
            upcxx_assert_msg(index%block_sz==0, "blocriter must be constructed "
                    "with index at beginning of block");
    }

    blocriter(const blocriter &orig) : da(orig.da), idx(orig.idx), 
        block_sz(da->block_sz) {}

    blocriter& operator=(const blocriter &rhs) {
        da = rhs.da;
        idx = rhs.idx;
        block_sz = rhs.block_sz;
        return *this;
    }

    blocriter& operator++() {
        idx -= block_sz;
        return *this;
    }

    blocriter operator++(int) {
        blocriter old(*this);
        idx -= block_sz;
        return old;
    }

    blocriter& operator--() {
        idx += block_sz;
        return *this;
    }

    blocriter operator--(int) {
        blocriter old(*this);
        idx += block_sz;
        return old;
    }

    blocriter& operator+=(const ssize_t shift) {
        idx -= block_sz*shift;
        return *this;
    }

    blocriter& operator-=(const ssize_t shift) {
        idx += block_sz*shift;
        return *this;
    }

    future<global_ptr<T>> operator* () const
    { return da->ptr(idx); }
    deref_helper<future<global_ptr<T>>> operator->() const {
        future<global_ptr<T>> f = da->ptr(idx); 
        return deref_helper<future<global_ptr<T>>>(f);
    }
    future<global_ptr<T>> operator[] (const ssize_t index) const
    { return da->ptr(idx-block_sz*index); }

    bool                  operator == (const blocriter &rhs) const
    { return idx == rhs.idx; }
    bool                  operator != (const blocriter &rhs) const
    { return idx != rhs.idx; }
    bool                  operator <  (const blocriter &rhs) const
    { return idx <  rhs.idx; }
    bool                  operator >  (const blocriter &rhs) const
    { return idx >  rhs.idx; }
    bool                  operator <= (const blocriter &rhs) const
    { return idx <= rhs.idx; }
    bool                  operator >= (const blocriter &rhs) const
    { return idx >= rhs.idx; }

    difference_type       operator -  (const blocriter &rhs) const
    { return (rhs.idx-idx)/block_sz; }
    blocriter             operator +  (const ssize_t shift) const 
    { return blocriter(this->da, this->idx-block_sz*shift); }
    blocriter             operator -  (const ssize_t shift) const 
    { return blocriter(this->da, this->idx+block_sz*shift); }
    friend blocriter       operator + (difference_type lhs, const blocriter& rhs)
    { return blocriter(rhs.idx + lhs*rhs.block_sz); }
    friend blocriter       operator - (difference_type lhs, const blocriter& rhs)
    { return blocriter(rhs.idx - lhs*rhs.block_sz); }

    size_t block_size() const { 
      if ((size_t)idx < full) { // cast intentionally wraps negative values
        return block_sz;
      } else if ((size_t)idx < da->N) {
        return trail;
      } else { 
        return 0; // iterator is out-of-bounds
      }
    }

private:
    dist_array *da;
    ssize_t idx;
    size_t block_sz, full, trail;
};

class info {
public:
    info(size_t global_idx, size_t global_block, size_t local_idx, 
         size_t local_block, intrank_t rank, size_t phase)
        : gi(global_idx), gb(global_block), li(local_idx), lb(local_block),
          r(rank), p(phase) {}
    size_t global_idx() { return gi; }
    size_t global_block() { return gb; }
    size_t process_idx() { return li; }
    size_t process_block() { return lb; }
    intrank_t rank() { return r; }
    size_t phase() { return p; }

private:
    size_t gi, gb, li, lb, p;
    intrank_t r;
};

class process_array {
public:
    using value_type = T;
    using reference = T&;
    using const_reference = const T&;
    using iterator = T*;
    using const_iterator = const T*;
    using difference_type = std::ptrdiff_t;
    using size_type = std::size_t;
    process_array(dist_array *da)
        : m_begin(da->data()), m_end(da->data() + da->process_size()) {}
    iterator begin() const { return m_begin; }
    iterator end() const { return m_end; }
    const_iterator cbegin() const { return m_begin; }
    const_iterator cend() const { return m_end; }
    size_t size() const { return distance(m_begin, m_end); }
    bool empty() const { return m_begin==m_end; }

private:
    T *m_begin, *m_end;
};

enum class replace_method : int { RAND };

class params {
public:
    params(size_t size=world().rank_n(), replace_method replace=replace_method::RAND) 
        : N(size), swap(replace) {
            upcxx_assert_msg(size>0, "cache must have a positive size");
        }
    size_t size()
    { return N; }
    void resize(size_t size)
    { N = size; }
    replace_method replace()
    { return swap; }
    void replace(replace_method m)
    { swap = m; }
private:
    size_t N;
    replace_method swap;
};

protected:
class cache {
public:
    cache(params p) : swap(p.replace()), list(p.size()), N(p.size()), dist(0,N-1)
    {}

    global_ptr<T> find(const intrank_t target) {
        typename std::unordered_map<intrank_t,global_ptr<T>>::const_iterator elem;
        if (thread_safe) {
            std::lock_guard<std::mutex> lg(mut);
            elem = list.find(target);
            if (elem == list.end()) return global_ptr<T>(nullptr);
            return elem->second;
        } else {
            elem = list.find(target);
            if (elem == list.end()) return global_ptr<T>(nullptr);
            return elem->second;
        }
    }

    void insert(intrank_t target, global_ptr<T> gptr) {
        upcxx_assert_msg(list.size()<N, "cache has no room for insertion");
        if (thread_safe) {
            std::lock_guard<std::mutex> lg(mut);
            list.insert({target,gptr});
        } else
            list.insert({target,gptr});
    }

    void erase(size_t index) {
        upcxx_assert_msg(index<size(), "specified index is beyond "
                "the bounds of the cache");
        if (thread_safe) {
            std::lock_guard<std::mutex> lg(mut);
            list.erase(std::next(list.begin(), index));
        } else
            list.erase(std::next(list.begin(), index));
    }

    void update(const intrank_t target, const global_ptr<T> gptr) {
        if (thread_safe) {
            std::lock_guard<std::mutex> lg(mut);
            auto elem = list.find(target);
            if (elem == list.end()) {
                if (list.size() == N)
                    list.erase(std::next(list.begin(), dist(generator)));
                list.insert({target,gptr});
            }
        } else {
            if (!find(target)) {
                if (full())
                    erase(dist(generator));
                insert(target, gptr);
            }
        }
    }

    void fill(dist_array *da)
    {
        list[da->m_team.rank_me()] = da->A;
        future<> fut_all = make_future();
        for (intrank_t i = 0; i < da->m_team.rank_n(); i++) {
            future<> fut = broadcast(list[i], i, da->team())
                .then([this,i](const global_ptr<T> &val) { 
                        list[i] = val; 
                });
            fut_all = when_all(fut_all, fut);
            progress();
        }
        fut_all.wait();
    }

    replace_method replace() const { return swap; }

    size_t capacity() const { return N; }

    size_t size() const { 
        if (thread_safe) {
            auto change = const_cast<cache*>(this);
            change->mut.lock();
            size_t val = list.size();
            change->mut.unlock();
            return val;
        } else
            return list.size(); 
    }

    bool full() const { 
        upcxx_assert_msg(list.size()<=N, "cache has grown beyond capacity");
        if (thread_safe) {
            auto change = const_cast<cache*>(this);
            std::lock_guard<std::mutex> lg(change->mut);
            return (list.size() == N);
        } else
            return list.size() == N; 
    }

    std::unordered_map<intrank_t,std::list<std::pair<persona*,promise<global_ptr<T>>*>>> want;
    std::mutex tex; // enables exclusive access to the cache of pending fetches for base pointers

protected:
    std::unordered_map<intrank_t,global_ptr<T>> list;

private:
    size_t N;
    replace_method swap;
    std::uniform_int_distribution<> dist;
    std::default_random_engine generator;
    std::mutex mut; // enables exclusive access to the cache of base pointers
};

private:
dist_object<global_ptr<T>> *DA;
global_ptr<T> A;
size_t N, block_sz, gbl_blocks, loc_size, loc_blocks, full, trail;
class team &m_team;
class cache m_cache;
};

} } // end of upcxx and extra namespaces

#endif
