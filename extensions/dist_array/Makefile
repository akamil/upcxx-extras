# This Makefile demonstrates the recommended way to build simple UPC++ programs.
# Note this uses some GNU make extensions for conciseness.
#
# To use this makefile, set the UPCXX_INSTALL variable to the upcxx install directory, e.g.
# make UPCXX_INSTALL=<myinstalldir> hello-world
# or (for bash)
# export UPCXX_INSTALL=<myinstalldir>; make hello-world

ifeq ($(UPCXX_INSTALL),)
$(warning UPCXX_INSTALL environment variable is not set, assuming upcxx is in the PATH)
UPCXX=upcxx
UPCXXRUN=upcxx-run
else
ifeq ($(wildcard $(UPCXX_INSTALL)/bin/upcxx),)
$(error Please set UPCXX_INSTALL=/path/to/upcxx/install)
else
UPCXX=$(UPCXX_INSTALL)/bin/upcxx
UPCXXRUN=$(UPCXX_INSTALL)/bin/upcxx-run
endif
endif

UPCXX_THREADMODE ?= seq
export UPCXX_THREADMODE
UPCXX_CODEMODE ?= debug
export UPCXX_CODEMODE
CXX = $(UPCXX)

# Example-specific variable specializations.
PAR_EXAMPLES = \
  bin/DA-threads
$(PAR_EXAMPLES): UPCXX_THREADMODE=par

CXXFLAGS = # optional command-line override

TARGETS = $(patsubst examples/%.cpp, bin/%, $(wildcard examples/*.cpp))

all: $(TARGETS)

bin/%: examples/%.cpp upcxx-extras/dist_array.hpp
	-@mkdir -p bin
	$(CXX) $(CXXFLAGS) $< -o $@ -I.

PROCS ?= 4
NODES ?=
ARGS ?=
LINE = =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

run: $(TARGETS)
	@for file in $(TARGETS) ; do \
          if test -x $$file ; then \
            echo $(LINE) ; \
            ( set -x ; \
              $(UPCXXRUN) -n $(PROCS) $$file $(ARGS) ; \
            ) ; \
            echo $(LINE) ; \
          fi ; \
         done

.PHONY: all run clean

clean:
	rm -rf $(TARGETS)

