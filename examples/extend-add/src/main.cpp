#include <iostream>
#include <cmath>
#include <string>
#include <map>

#include "DistributedMatrix.hpp"
#include "FrontalMatrix.hpp"
#include "FrontalMatrixMPI.hpp"
#include "ExtendAdd.hpp"

#ifdef _PROXY_MPI_BACKEND_
#include "MPI_wrapper.hpp"
#include "scalapack.hpp"
#else
#include <upcxx/upcxx.hpp>
#include "blacs_upcxx.hpp"
#endif
#include "Utils.hpp"

int main( int argc, char ** argv) {
  using namespace std;
  using F_t = strumpack::FrontalMatrix<double,int>;
  using FMPI_t = strumpack::FrontalMatrixMPI<double,int>;

#ifdef _PROXY_MPI_BACKEND_
  MPI_Init(&argc,&argv);
  int P;
  MPI_Comm_size(MPI_COMM_WORLD, &P);
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#else
  upcxx::init();
  int P = upcxx::rank_n();
  int rank = upcxx::rank_me();
#endif

  //Check if an input file
  if ( argc != 2 ) {
    if (rank == 0) {
      std::cerr<<"Wrong number of arguments provided."<<std::endl<<"Usage: "<<argv[0]<<" filename"<<std::endl;
    }
#ifdef _PROXY_MPI_BACKEND_
      MPI_Finalize();
#else
      upcxx::finalize();
#endif
      exit(1);
  }


  int nsep = 0;
  int max_orig_np = 0;
  std::stringstream fname;
  fname << "logfile" << rank;
  logfile.open(fname.str());

  logfile<<"rank is "<<rank<<std::endl<<"P is "<<P<<std::endl;

  int npcol = floor(sqrt((float)P));
  int nprow = P / npcol;

  //First, check if input file exists
  bool validInput = false;
  if (rank == 0) {
    std::ifstream fin(argv[1]);
    validInput = fin.good();
    if ( validInput ) {
      //check if this file is for the correct number of processes
      int numproc = -1;
      fin >> numproc;

      validInput = numproc == P;
      if ( !validInput ) std::cerr<<argv[1]<<" should be used with "<<numproc<<" processes."<<std::endl;
    }
    else {
      std::cerr<<argv[1]<<" is not a valid input file."<<std::endl;
    }
  }

#ifdef _PROXY_MPI_BACKEND_
  MPI_Bcast(&validInput,1,MPI_C_BOOL,0,MPI_COMM_WORLD);
  if ( !validInput ) {
      MPI_Finalize();
      exit(1);
  }
#else
  validInput = upcxx::broadcast(validInput,0).wait();
  if ( !validInput ) { 
      upcxx::finalize();
      exit(1);
  }
#endif


  if (rank == 0) {
    std::ifstream fin(argv[1]);
    std::string str;
    std::vector<int> idx;
    int numproc;
    int sep, lc,rc, sep_beg, sep_end , idx_cnt, val, gfp, fp, np;
    fin.clear();
    fin.seekg(0,std::ios::beg);
    fin >> numproc;
    //skip the rest of the line
    fin.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
    //skip the header
    fin.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );

    while ( fin >> sep >> gfp >> fp>> np >> lc >> rc >> sep_beg >> sep_end >>idx_cnt ) {
      idx.clear();
      idx.reserve(idx_cnt);
      while ( idx_cnt-- > 0 && fin >> val ) {idx.push_back(val);}
      max_orig_np = std::max(max_orig_np,np);
      nsep++;
    }
  }

#ifdef _PROXY_MPI_BACKEND_
  MPI_Bcast(&max_orig_np,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&nsep,1,MPI_INT,0,MPI_COMM_WORLD);
#else
  max_orig_np = upcxx::broadcast(max_orig_np,0).wait();
  nsep = upcxx::broadcast(nsep,0).wait();
#endif

  double pratio = (double)P / (double)max_orig_np;

  if (npcol*nprow!=P) {
    npcol = P;
    nprow = 1;
  }

  if (!rank) {
    std::cout<<"nprow is "<<nprow<<std::endl;
    std::cout<<"npcol is "<<npcol<<std::endl;
  }

#ifdef _PROXY_MPI_BACKEND_
  int ctxt;
  Cblacs_get(0, 0, &ctxt);

  if ( ctxt==-1) abort();

  Cblacs_gridinit(&ctxt, "C", nprow, npcol);
#endif
  F_t * root = nullptr;
  std::vector< F_t * > front_ptr;
  std::vector< std::tuple<int, int> > front_chld;
  std::vector< int > front_parent;

  {
    int pactive = nprow*npcol;
#ifdef _PROXY_MPI_BACKEND_
    auto main_team =  Cblacs2sys_handle(ctxt);
#else
    auto & main_team =  upcxx::world();
#endif


    {
      scope_timer a("frontal_matrix_creation");


      if ( rank < nprow*npcol ) {
        front_chld.resize(nsep, std::make_tuple(-1,-1));
        front_parent.resize(nsep,-1);
        front_ptr.resize(nsep,nullptr);
        pratio = (double)pactive / (double)max_orig_np;
      }

      {
        std::ifstream fin(argv[1]);
        std::string str;
        std::vector<int> idx;
        int sep, lc,rc, sep_beg, sep_end , idx_cnt, val, gfp, fp, np;
        int isep = 0;
        fin.clear();
        fin.seekg(0,std::ios::beg);
        int numproc;
        fin >> numproc;
        //skip the rest of the line
        fin.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
        //skip the header
        fin.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
        while ( fin >> sep >> gfp >> fp>> np >> lc >> rc >> sep_beg >> sep_end >>idx_cnt ) {
          idx.clear();
          idx.reserve(idx_cnt);
          while ( idx_cnt-- > 0 && fin >> val ) {idx.push_back(val);} 
          isep++;


          int orig_np = np;
          int orig_fp = fp;
          double local_pratio = (double)orig_fp / (double)orig_np;
          assert(lc!=rc || lc==-1);

          front_chld[sep] = std::make_tuple(lc,rc);
          if (lc!=-1) front_parent[lc] = sep;
          if (rc!=-1) front_parent[rc] = sep;

          //_sep is supernode id
          //_seb_begin is index of first col of this front
          //_seb_end is index of last col of this front
          //_dim_upd is size of column indexes _upd
          //_upd is the column indices updated by this supernode
          //_front_comm is the total communicator holding the entire frontal matrix (can be larger than the associated context)
          //_total_procs is the size of that _front_comm

          //get pointer to parent
          auto parent = front_parent[sep];

          F_t * parent_ptr = nullptr;
          if (parent != -1) {
            parent_ptr = front_ptr[parent]; 
            //parent is not null but since the parent is nullptr, it 
            //means that there is no need to create that frontalmatrix
            if ( parent_ptr == nullptr) continue;
            if ( auto pa_mpi = dynamic_cast<FMPI_t*>(parent_ptr)) {
#ifdef _PROXY_MPI_BACKEND_
              if ( pa_mpi->comm() == MPI_COMM_NULL) continue;
#else
              if ( pa_mpi->visit() == false) continue;
#endif
            }
          }

          if ( pactive != max_orig_np ) np = pratio * orig_np; 

          F_t * pFMat = nullptr;
          if ( np>1) {
            auto pa_mpi = dynamic_cast<FMPI_t*>(parent_ptr);
            assert( (isep==1 && parent_ptr==nullptr) || pa_mpi!=nullptr);
#ifdef _PROXY_MPI_BACKEND_
            auto pteam = main_team;
            if (parent != -1) pteam = pa_mpi->comm();

            int parent_size = mpi_nprocs(pteam);
            if ( pactive != max_orig_np ) np = std::min(np,parent_size);
            if ( pactive != max_orig_np ) fp = int(local_pratio *orig_fp) % np;
            assert( parent==-1 || np <=parent_size);
#else
            auto * pteam = &main_team;
            if (parent != -1) pteam = &pa_mpi->team();
#endif

#ifdef _PROXY_MPI_BACKEND_
            auto team = pteam;
            if (pteam != MPI_COMM_NULL) team = mpi_sub_comm(pteam,fp,np);
            int prank = mpi_rank(pteam);
            assert( (prank<fp || prank>=fp+np) || team!=MPI_COMM_NULL); 
#else
            int color = (*pteam).rank_me()>=fp && (*pteam).rank_me()<fp+np;
            int key = (*pteam).rank_me();
            upcxx::team team = (*pteam).split(color,key);
#endif

#ifdef _PROXY_MPI_BACKEND_
            pFMat = new FMPI_t(  sep, sep_beg, sep_end, idx.size(),idx.data(),team,np);
#else
            pFMat = new FMPI_t(  sep, sep_beg, sep_end, idx.size(),idx.data(),team,color==1,np);
            team.destroy();
#endif

          } 
          else {
            pFMat = new F_t(  nullptr, nullptr, sep, sep_beg, sep_end, idx.size(),idx.data());
          }

          front_ptr[sep] = pFMat;
          if (isep==1) root = pFMat;
        }
      }

      if ( rank < nprow*npcol ) {
        for (auto & ptr: front_ptr) {
          if (ptr) {
            auto children = front_chld[ptr->sep];
            auto lc = std::get<0>(children);
            auto rc = std::get<1>(children);
            F_t * lcp = nullptr, * rcp = nullptr;
            if (lc!=-1) lcp = front_ptr[lc];
            if (rc!=-1) rcp = front_ptr[rc];
            if (auto pa_mpi = dynamic_cast<FMPI_t*>(ptr)) {
              pa_mpi->setChildren(lcp,rcp);
            }
            else if (auto pa_seq = dynamic_cast<F_t*>(ptr)) {
              pa_seq->lchild = lcp;
              pa_seq->rchild = rcp;
            }
          }
        }
      }
    }
  }  
#ifndef _PROXY_MPI_BACKEND_
  upcxx::barrier();
#else
  MPI_Barrier(MPI_COMM_WORLD);
#endif

  {
    scope_timer b("multifrontal_factorization");
    if ( rank < nprow*npcol ) root->multifrontal_factorization(0,0);
  }

  delete root;

  logfile.close();

  if (rank == 0 ) std::cout<<"SUCCESS"<<std::endl;
#ifdef _PROXY_MPI_BACKEND_
  Cblacs_exit(1);
  MPI_Finalize();
#else
  upcxx::finalize();
#endif

  return 0;
}

