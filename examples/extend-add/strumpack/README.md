Source code in this directory is extracted from the [UPCProxy](https://github.com/pghysels/STRUMPACK/tree/UPCProxy) 
branch of STRUMPACK, which license is available in strumpack/license.txt.
