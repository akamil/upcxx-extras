/*
 * STRUMPACK -- STRUctured Matrices PACKage, Copyright (c) 2014, The
 * Regents of the University of California, through Lawrence Berkeley
 * National Laboratory (subject to receipt of any required approvals
 * from the U.S. Dept. of Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this
 * software, please contact Berkeley Lab's Technology Transfer
 * Department at TTD@lbl.gov.
 *
 * NOTICE. This software is owned by the U.S. Department of Energy. As
 * such, the U.S. Government has been granted for itself and others
 * acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative
 * works, and perform publicly and display publicly.  Beginning five
 * (5) years after the date permission to assert copyright is obtained
 * from the U.S. Department of Energy, and subject to any subsequent
 * five (5) year renewals, the U.S. Government is granted for itself
 * and others acting on its behalf a paid-up, nonexclusive,
 * irrevocable, worldwide license in the Software to reproduce,
 * prepare derivative works, distribute copies to the public, perform
 * publicly and display publicly, and to permit others to do so.
 *
 * Developers: Pieter Ghysels, Francois-Henry Rouet, Xiaoye S. Li.
 *             (Lawrence Berkeley National Lab, Computational Research
 *             Division).
 *
 */
#ifndef FRONTAL_MATRIX_HPP
#define FRONTAL_MATRIX_HPP

#include <iostream>
#include <algorithm>
#include <random>
#include <vector>
#include "blas_lapack_wrapper.hpp"
#include "strumpack_parameters.hpp"
#include "SPOptions.hpp"
#if defined(_OPENMP)
#include "omp.h"
#endif
#include "DenseMatrix.hpp"

namespace strumpack {

  // forward declaration
  template<typename scalar_t> class DistributedMatrix;

  template<typename scalar_t,typename integer_t> class FrontalMatrix {
    using DistM_t = DistributedMatrix<scalar_t>;
    using F_t = FrontalMatrix<scalar_t,integer_t>;
    using DM_t = DenseMatrix<scalar_t>;
  public:
    integer_t sep;
    integer_t sep_begin;
    integer_t sep_end;
    integer_t dim_sep;
    integer_t dim_upd;
    integer_t dim_blk; // = dim_sep + dim_upd
    integer_t* upd;   // TODO store as a vector

    F_t* lchild;
    F_t* rchild;

    DM_t F11;
    DM_t F21;
    DM_t F12;
    DM_t F22;

    // TODO this pointer should not be stored in every front,
    // instead just pass it around whenever needed?
    integer_t p_wmem;

    FrontalMatrix(integer_t separator,
                  F_t* left_child, F_t* right_child);
    FrontalMatrix(
                  F_t* _lchild, F_t* _rchild,
                  integer_t _sep, integer_t _sep_begin, integer_t _sep_end,
                  integer_t _dim_upd, integer_t* _upd);
    virtual ~FrontalMatrix();

    void find_upd_indices(const std::vector<std::size_t>& I,
                          std::vector<std::size_t>& lI,
                          std::vector<std::size_t>& oI) const;
    std::vector<std::size_t> upd_to_parent(const F_t* pa,
                                           std::size_t& upd2sep) const;
    std::vector<std::size_t> upd_to_parent(const F_t* pa) const;

    virtual void multifrontal_factorization(int etree_level=0, int task_depth=0);

    virtual void release_work_memory();


    void extend_add_to_dense(FrontalMatrix<scalar_t,integer_t>* p, int task_depth);

    virtual integer_t maximum_rank(int task_depth=0) const { return 0; }
    virtual long long factor_nonzeros(int task_depth=0) const;
    virtual long long dense_factor_nonzeros(int task_depth=0) const;
    virtual bool isMPI() const { return false; }
    virtual std::string type() const { return "FrontalMatrix"; }
    void permute_upd_indices(integer_t* perm, int task_depth=0);

  protected:
    FrontalMatrix(const FrontalMatrix&) = delete;
    FrontalMatrix& operator=(FrontalMatrix const&) = delete;
    virtual long long node_factor_nonzeros() const
    { return dim_blk*dim_blk-dim_upd*dim_upd; }
    virtual long long dense_node_factor_nonzeros() const
    { return node_factor_nonzeros(); }
  };

  template<typename scalar_t,typename integer_t>
  FrontalMatrix<scalar_t,integer_t>::FrontalMatrix
  (integer_t separator,
   F_t* left_child, F_t* right_child) :
    sep(separator), sep_begin(0), sep_end(0), dim_sep(0),
    dim_upd(0), dim_blk(0), upd(NULL),
    lchild(left_child), rchild(right_child), p_wmem(0) {
  }

  template<typename scalar_t,typename integer_t>
  FrontalMatrix<scalar_t,integer_t>::FrontalMatrix
  ( F_t* _lchild, F_t* _rchild,
   integer_t _sep, integer_t _sep_begin, integer_t _sep_end,
   integer_t _dim_upd, integer_t* _upd)
    : sep(_sep), sep_begin(_sep_begin), sep_end(_sep_end),
      dim_sep(_sep_end - _sep_begin), dim_upd(_dim_upd),
      dim_blk(_sep_end - _sep_begin + _dim_upd),
      lchild(_lchild), rchild(_rchild), p_wmem(0) {
    upd = new integer_t[dim_upd];
    std::copy(_upd, _upd+dim_upd, upd);
  }

  template<typename scalar_t,typename integer_t>
  FrontalMatrix<scalar_t,integer_t>::~FrontalMatrix() {
    delete[] upd;
    delete lchild;
    delete rchild;
  }


  /**
   * I[0:n-1] contains a list of unsorted global indices. This routine
   * finds the corresponding indices into the upd part of this front
   * and puts those indices in lI. Not all indices from I need to be
   * in lI, so oI stores for each element in lI, to which element in I
   * that element corresponds.
   */
  template<typename scalar_t,typename integer_t> void
  FrontalMatrix<scalar_t,integer_t>::find_upd_indices
  (const std::vector<std::size_t>& I, std::vector<std::size_t>& lI,
   std::vector<std::size_t>& oI) const {
    auto n = I.size();
    lI.reserve(n);
    oI.reserve(n);
    for (std::size_t i=0; i<n; i++) {
      auto l = std::lower_bound(upd, upd+dim_upd, I[i]);
      if (l != upd+dim_upd && *l == int(I[i])) {
        lI.push_back(l-upd);
        oI.push_back(i);
      }
    }
  }

  template<typename scalar_t,typename integer_t> std::vector<std::size_t>
  FrontalMatrix<scalar_t,integer_t>::upd_to_parent
  (const F_t* pa) const {
    std::size_t upd2sep;
    return upd_to_parent(pa, upd2sep);
  }

  template<typename scalar_t,typename integer_t> std::vector<std::size_t>
  FrontalMatrix<scalar_t,integer_t>::upd_to_parent
  (const F_t* pa, std::size_t& upd2sep) const {
    std::vector<std::size_t> I(dim_upd);
    integer_t r = 0;
    for (; r<dim_upd; r++) {
      auto up = upd[r];
      if (up >= pa->sep_end) break;
      I[r] = up - pa->sep_begin;
    }
    upd2sep = r;
    for (integer_t t=0; r<dim_upd; r++) {
      auto up = upd[r];
      while (pa->upd[t] < up) t++;
      I[r] = t + pa->dim_sep;
    }
    return I;
  }

  template<typename scalar_t,typename integer_t> long long
  FrontalMatrix<scalar_t,integer_t>::factor_nonzeros(int task_depth) const {
    long long nnz = node_factor_nonzeros(), nnzl = 0, nnzr = 0;
    if (lchild)
#pragma omp task default(shared)                        \
  if(task_depth < params::task_recursion_cutoff_level)
      nnzl = lchild->factor_nonzeros(task_depth+1);
    if (rchild)
#pragma omp task default(shared)                        \
  if(task_depth < params::task_recursion_cutoff_level)
      nnzr = rchild->factor_nonzeros(task_depth+1);
#pragma omp taskwait
    return nnz + nnzl + nnzr;
  }

  template<typename scalar_t,typename integer_t> long long
  FrontalMatrix<scalar_t,integer_t>::dense_factor_nonzeros
  (int task_depth) const {
    long long nnz = dense_node_factor_nonzeros(), nnzl = 0, nnzr = 0;
    if (lchild)
#pragma omp task default(shared)                        \
  if(task_depth < params::task_recursion_cutoff_level)
      nnzl = lchild->dense_factor_nonzeros(task_depth+1);
    if (rchild)
#pragma omp task default(shared)                        \
  if(task_depth < params::task_recursion_cutoff_level)
      nnzr = rchild->dense_factor_nonzeros(task_depth+1);
#pragma omp taskwait
    return nnz + nnzl + nnzr;
  }

  template<typename scalar_t,typename integer_t> void
  FrontalMatrix<scalar_t,integer_t>::permute_upd_indices
  (integer_t* perm, int task_depth) {
    auto lch = lchild; auto rch = rchild;
    if (lch)
#pragma omp task default(shared) firstprivate(perm,task_depth,lch)      \
  if(task_depth < params::task_recursion_cutoff_level)
      lch->permute_upd_indices(perm, task_depth+1);
    if (rch)
#pragma omp task default(shared) firstprivate(perm,task_depth,rch)      \
  if(task_depth < params::task_recursion_cutoff_level)
      rch->permute_upd_indices(perm, task_depth+1);

    for (integer_t i=0; i<dim_upd; i++) upd[i] = perm[upd[i]];
    std::sort(upd, upd+dim_upd);
#pragma omp taskwait
  }

  template<typename scalar_t,typename integer_t> void
    FrontalMatrix<scalar_t,integer_t>::multifrontal_factorization
    (int etree_level, int task_depth) {
      if (this->lchild)
        this->lchild->multifrontal_factorization(etree_level+1, task_depth);
      if (this->rchild)
        this->rchild->multifrontal_factorization(etree_level+1, task_depth);

      // TODO can we allocate the memory in one go??
      const auto dsep = this->dim_sep;
      const auto dupd = this->dim_upd;
      F11 = DM_t(dsep, dsep); //F11.zero();
      F12 = DM_t(dsep, dupd); //F12.zero();
      F21 = DM_t(dupd, dsep); //F21.zero();

      //A.extract_front
      //  (F11, F12, F21, this->sep_begin, this->sep_end, this->upd, task_depth);

      if (dupd) {
        F22 = DM_t(dupd, dupd);
        //F22.zero();
      }

      if (this->lchild) this->lchild->extend_add_to_dense(this, task_depth);
      if (this->rchild) this->rchild->extend_add_to_dense(this, task_depth);


      //computation phase
      if (this->dim_sep) {
        auto piv = F11.LU(task_depth);
        if (this->dim_upd) {
          //TODO This is where computations go
        }
      }
    }

  template<typename scalar_t,typename integer_t> void
  FrontalMatrix<scalar_t,integer_t>::extend_add_to_dense
  (FrontalMatrix<scalar_t,integer_t>* p, int task_depth) {
    const std::size_t pdsep = p->dim_sep;
    const std::size_t dupd = this->dim_upd;
    std::size_t upd2sep;
    auto I = this->upd_to_parent(p, upd2sep);
    for (std::size_t c=0; c<dupd; c++) {
      auto pc = I[c];
      if (pc < pdsep) {
        for (std::size_t r=0; r<upd2sep; r++)
          p->F11(I[r],pc) += F22(r,c);
        for (std::size_t r=upd2sep; r<dupd; r++)
          p->F21(I[r]-pdsep,pc) += F22(r,c);
      } else {
        for (std::size_t r=0; r<upd2sep; r++)
          p->F12(I[r],pc-pdsep) += F22(r, c);
        for (std::size_t r=upd2sep; r<dupd; r++)
          p->F22(I[r]-pdsep,pc-pdsep) += F22(r,c);
      }
    }
    release_work_memory();
  }





  template<typename scalar_t,typename integer_t> void
    FrontalMatrix<scalar_t,integer_t>::release_work_memory() {
      F22.clear(); // remove the update block
    }
} // end namespace strumpack

#endif //FRONTAL_MATRIX_HPP
