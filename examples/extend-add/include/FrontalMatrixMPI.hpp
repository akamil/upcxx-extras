/*
 * Portions of the code in this file are adapted from STRUMPACK, see strumpack/license.txt
 *
 */
#ifndef FRONTAL_MATRIX_MPI_HPP
#define FRONTAL_MATRIX_MPI_HPP

#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <array>
#include <cmath>
#include "DistributedMatrix.hpp"
#include "ExtendAdd.hpp"

#ifdef _PROXY_MPI_BACKEND_
#include "MPI_wrapper.hpp"
#else
#include <upcxx/upcxx.hpp>
#include "blacs_upcxx.hpp"
#endif
#include "Utils.hpp"

namespace strumpack {


  template<typename scalar_t,typename integer_t>
    class FrontalMatrixMPI : public FrontalMatrix<scalar_t,integer_t> {
      using FMPI_t = FrontalMatrixMPI<scalar_t,integer_t>;
      using F_t = FrontalMatrix<scalar_t,integer_t>;
      using DistM_t = DistributedMatrix<scalar_t>;
      using ExtAdd = ExtendAdd<scalar_t,integer_t>;
      template<typename _scalar_t,typename _integer_t> friend class ExtendAdd;

#ifndef _PROXY_MPI_BACKEND_
      using Dist_FMPI_t = upcxx::dist_object<FMPI_t*>;
#endif

      public:


#ifdef _PROXY_MPI_BACKEND_
      struct ch_offset_data{ int *upd_r_1, *upd_c_1, *upd_r_2, *upd_c_2; int *r_1, *c_1, *r_2, *c_2; integer_t r_max_1, r_max_2, c_max_1, c_max_2; };
#endif

      FrontalMatrixMPI(
          integer_t _sep, integer_t _sep_begin, integer_t _sep_end,
          integer_t _dim_upd, integer_t* _upd, 
#ifdef _PROXY_MPI_BACKEND_
          MPI_Comm _front_comm,
#else
          upcxx::team & _front_team,
          bool aactive,
#endif
          int _total_procs);
      void setChildren( F_t* alchild, F_t* archild);
      FrontalMatrixMPI(const FrontalMatrixMPI&) = delete;
      FrontalMatrixMPI& operator=(FrontalMatrixMPI const&) = delete;
      virtual ~FrontalMatrixMPI();
      inline bool visit(const F_t* ch) const;

      void release_work_memory();
      void multifrontal_factorization(int etree_level, int task_depth);
      void partial_factorization();
      inline int child_master(const F_t* ch) const;
      inline bool active() const {
#ifdef _PROXY_MPI_BACKEND_
        return front_comm != MPI_COMM_NULL &&
          mpi_rank(front_comm) < proc_rows*proc_cols; 
#else
        return bactive && (front_team.rank_me() < proc_rows*proc_cols);
#endif
      }

      inline bool visit() const {
#ifdef _PROXY_MPI_BACKEND_
        return front_comm != MPI_COMM_NULL;
#else
        return bactive;
#endif
      }


      static inline void processor_grid(int np_procs, int& np_rows, int& np_cols) {
        np_cols = std::floor(std::sqrt((float)np_procs));
        np_rows = np_procs / np_cols;
      }
      inline int np_rows() const { return proc_rows; }
      inline int np_cols() const { return proc_cols; }
      inline int find_rank(integer_t r, integer_t c, const DistM_t& F) const;
      inline int find_rank_fixed(integer_t r, integer_t c,
          const DistM_t& F) const;
#ifdef _PROXY_MPI_BACKEND_
      virtual long long dense_factor_nonzeros(int task_depth=0) const;
#endif
      virtual std::string type() const { return "FrontalMatrixMPI"; }
      virtual bool isMPI() const { return true; }
#ifdef _PROXY_MPI_BACKEND_
      MPI_Comm comm() const { return front_comm; }
#else    
      upcxx::team & team() { return front_team; }
#endif

#ifdef _PROXY_MPI_BACKEND_
      inline int blacs_context() const { return ctxt; }
      inline int blacs_context_all() const { return ctxt_all; }
#endif

      //This is coming from FrontalMatrixDenseMPI
      void extend_add();
      void build_front();
      void print_fronts();

      protected:
#ifdef _PROXY_MPI_BACKEND_
      // this is a blacs context with only the active process for this front
      int ctxt;
      // this is a blacs context with all process for this front
      int ctxt_all;
#endif
      // number of processes per row in the blacs ctxt
      int proc_rows;
      // number of processes per col in the blacs ctxt
      int proc_cols;
      // number of processes that work on the subtree belonging to this front,
      int total_procs;
      // this can be more than the processes in the blacs context ctxt
      // and is not necessarily the same as mpi_nprocs(front_comm),
      // because if this rank is not part of front_comm,
      // mpi_nprocs(front_comm) == 0
      std::array< std::vector<int> , 2 > pa_offsets_container;
      struct pa_offset_data { int * pr, *pc; int r_upd, c_upd; };
      std::array< pa_offset_data, 2 > pa_offsets_ptr;

#ifdef _PROXY_MPI_BACKEND_
      MPI_Comm front_comm;  // MPI communicator for this front
      std::array< std::vector<int> , 2 > ch_offsets_container;
      std::array< ch_offset_data, 2 > ch_offsets_ptr;
#ifdef _PROXY_MPI_P2P_
      std::array< std::vector< std::vector<int> >, 2 > ch_row_offsets;
      std::array< std::vector< std::vector<int> >, 2 > ch_col_offsets;
      std::vector<MPI_Request> recv_requests;
      std::vector<MPI_Request> send_requests;
      std::vector<MPI_Status> send_statuses;
      std::vector<MPI_Status> recv_statuses;
#endif

#else
      bool bactive;
      upcxx::team * active_front_team;
      upcxx::team front_team;
      std::array<upcxx::promise<>,2> extend_add_prom;
      std::array< std::vector< std::vector<int> >, 2 > ch_row_offsets;
      std::array< std::vector< std::vector<int> >, 2 > ch_col_offsets;
#endif
      //This is coming from FrontalMatrixDenseMPI
      DistM_t F11, F12, F21, F22;
    };

  template<typename scalar_t,typename integer_t>
    FrontalMatrixMPI<scalar_t,integer_t>::FrontalMatrixMPI
    (integer_t _sep,
     integer_t _sep_begin, integer_t _sep_end, integer_t _dim_upd,
     integer_t* _upd, 
#ifdef _PROXY_MPI_BACKEND_
     MPI_Comm _front_comm,
#else
     upcxx::team & _front_team, 
     bool aactive,
#endif
     int _total_procs)
    : F_t( NULL, NULL,_sep, _sep_begin, _sep_end, _dim_upd, _upd),
    total_procs(_total_procs), 
#ifdef _PROXY_MPI_BACKEND_
    front_comm(_front_comm)
#else
      bactive(aactive),
    active_front_team(nullptr),
    front_team(_front_team.split(1,_front_team.rank_me()))
#endif
    {

      processor_grid(total_procs, proc_rows, proc_cols);
#ifdef _PROXY_MPI_BACKEND_
      if (front_comm != MPI_COMM_NULL) {
        int active_procs = proc_rows * proc_cols;
        if (active_procs < total_procs) {
          auto active_front_comm = mpi_sub_comm(front_comm, 0, active_procs);
          if (mpi_rank(front_comm) < active_procs) {
            ctxt = Csys2blacs_handle(active_front_comm);
            Cblacs_gridinit(&ctxt, "C", proc_rows, proc_cols);
          } else ctxt = -1;
          mpi_free_comm(&active_front_comm);
        } else {
          ctxt = Csys2blacs_handle(front_comm);
          Cblacs_gridinit(&ctxt, "C", proc_rows, proc_cols);
        }
        ctxt_all = Csys2blacs_handle(front_comm);
        Cblacs_gridinit(&ctxt_all, "R", 1, total_procs);

#ifdef _PROXY_MPI_P2P_
        auto P = mpi_nprocs(this->front_comm);
        recv_requests.resize(2*P,MPI_REQUEST_NULL);
        send_requests.resize(2*P,MPI_REQUEST_NULL);
        send_statuses.resize(2*P);
        recv_statuses.resize(2*P);
#endif
      } else ctxt = ctxt_all = -1;
#else
      if (bactive) {
        if (total_procs>0 && front_team.rank_n() > 0 ) {
          int active_procs = proc_rows * proc_cols;
          if (active_procs < total_procs) {
            active_front_team = new upcxx::team(front_team.split(front_team.rank_me()<active_procs,front_team.rank_me()));
          } 
          else {
            active_front_team = &front_team;
          }
        } else {
          bactive = false; 
        }
      }
#endif
    }

  template<typename scalar_t,typename integer_t>
    void FrontalMatrixMPI<scalar_t,integer_t>::setChildren
    ( F_t* alchild, F_t* archild){

      this->lchild = alchild;
      this->rchild = archild;


#ifdef _PROXY_MPI_BACKEND_
      if (front_comm != MPI_COMM_NULL) {
        //pre compute mapping arrays
        int mycol,myrow,nprow,npcol;
        Cblacs_gridinfo(ctxt, &nprow, &npcol, &myrow, &mycol);
        int F11_lrows = scalapack::numroc(this->dim_sep, DistM_t::default_MB, myrow, 0, nprow);
        int F11_lcols = scalapack::numroc(this->dim_sep, DistM_t::default_NB, mycol, 0, npcol);
        int F22_lrows = scalapack::numroc(this->dim_upd, DistM_t::default_MB, myrow, 0, nprow);
        int F22_lcols = scalapack::numroc(this->dim_upd, DistM_t::default_NB, mycol, 0, npcol);
        auto rowl2g_fixed = [&](int row ) -> int {
          return indxl2g(row+1, DistM_t::default_MB, myrow, 0, nprow) - 1; };
        auto coll2g_fixed = [&](int col ) -> int {
          return indxl2g(col+1, DistM_t::default_NB, mycol, 0, npcol) - 1; };

        for (auto chbase : {this->lchild, this->rchild}) {
          if ( chbase != nullptr ) {
            if (auto ch = dynamic_cast<FMPI_t*>(chbase)) {
#ifdef _PROXY_MPI_P2P_
              auto & row_offsets = ch_row_offsets[ch==this->lchild?0:1];
              auto & col_offsets = ch_col_offsets[ch==this->lchild?0:1];
              row_offsets.resize(ch->proc_rows);
              col_offsets.resize(ch->proc_cols);

              bool pa_cb_active = myrow < nprow && mycol < npcol && (this->dim_upd || this->dim_sep) ;
              if ( pa_cb_active ) {
                const auto ch_dim_upd = ch->dim_upd;
                const auto ch_upd = ch->upd;
                const auto pa_upd = this->upd;
                const auto pa_sep = this->sep_begin;
                const auto prows = ch->proc_rows;
                const auto pcols = ch->proc_cols;
                const auto B = DistM_t::default_MB;

                for (int r=0, ur=0; r< F11_lrows; r++) {
                  auto fgr = rowl2g_fixed(r) + pa_sep;
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  row_offsets[ (ur / B) % prows ].push_back(r);
                }

                for (int r=0, ur=0; r< F22_lrows; r++) {
                  auto fgr = pa_upd[rowl2g_fixed(r)];
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  row_offsets[ (ur / B) % prows ].push_back(r);
                }

                for (int c=0, uc=0; c< F11_lcols; c++) {
                  auto fgc = coll2g_fixed(c) + pa_sep;
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  col_offsets[ (uc / B) % pcols ].push_back(c);
                }

                for (int c=0, uc=0; c< F22_lcols; c++) {
                  auto fgc = pa_upd[coll2g_fixed(c)];
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  col_offsets[ (uc / B) % pcols ].push_back(c);
                }
              }
#else
              auto & ch_container = ch_offsets_container[ch==this->lchild?0:1];
              auto & ch_ptr = ch_offsets_ptr[ch==this->lchild?0:1];
              bool pa_cb_active = myrow < nprow && mycol < npcol && (this->dim_upd || this->dim_sep) ;
              if ( pa_cb_active ) {
                const auto ch_dim_upd = ch->dim_upd;
                const auto ch_upd = ch->upd;
                const auto pa_upd = this->upd;
                const auto pa_sep = this->sep_begin;
                const auto prows = ch->proc_rows;
                const auto pcols = ch->proc_cols;
                const auto B = DistM_t::default_MB;

                ch_container.resize(2*F11_lrows+2*F11_lcols+2*F22_lrows+2*F22_lcols);
                auto & upd_r_1 = ch_ptr.upd_r_1;
                auto & upd_c_1 = ch_ptr.upd_c_1;
                auto & upd_r_2 = ch_ptr.upd_r_2;
                auto & upd_c_2 = ch_ptr.upd_c_2;
                auto & r_1 = ch_ptr.r_1;
                auto & c_1 = ch_ptr.c_1;
                auto & r_2 = ch_ptr.r_2;
                auto & c_2 = ch_ptr.c_2;
                auto & r_max_1 = ch_ptr.r_max_1;
                auto & r_max_2 = ch_ptr.r_max_2;
                auto & c_max_1 = ch_ptr.c_max_1;
                auto & c_max_2 = ch_ptr.c_max_2;

                upd_r_1 = ch_container.data();
                upd_c_1 = upd_r_1 + F11_lrows;
                upd_r_2 = upd_c_1 + F11_lcols;
                upd_c_2 = upd_r_2 + F22_lrows;
                r_1 = upd_c_2 + F22_lcols;
                c_1 = r_1 + F11_lrows;
                r_2 = c_1 + F11_lcols;
                c_2 = r_2 + F22_lrows;
                r_max_1 = 0;
                r_max_2 = 0;
                c_max_1 = 0;
                c_max_2 = 0;

                for (int r=0, ur=0; r< F11_lrows; r++) {
                  auto fgr = rowl2g_fixed(r) + pa_sep;
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  r_1[r_max_1] = r;
                  upd_r_1[r_max_1++] = (ur / B) % prows;
                }

                for (int r=0, ur=0; r< F22_lrows; r++) {
                  auto fgr = pa_upd[rowl2g_fixed(r)];
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  r_2[r_max_2] = r;
                  upd_r_2[r_max_2++] = (ur / B) % prows;
                }

                for (int c=0, uc=0; c< F11_lcols; c++) {
                  auto fgc = coll2g_fixed(c) + pa_sep;
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  c_1[c_max_1] = c;
                  upd_c_1[c_max_1++] = ((uc / B) % pcols) * prows;
                }

                for (int c=0, uc=0; c< F22_lcols; c++) {
                  auto fgc = pa_upd[coll2g_fixed(c)];
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  c_2[c_max_2] = c;
                  upd_c_2[c_max_2++] = ((uc / B) % pcols) * prows;
                }
              }
#endif

              auto & pa_container = pa_offsets_container[ch==this->lchild?0:1];
              auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];
              if ( ch->active() ){

                int ch_mycol,ch_myrow,ch_nprow,ch_npcol;
                Cblacs_gridinfo(ch->ctxt, &ch_nprow, &ch_npcol, &ch_myrow, &ch_mycol);

                bool ch_cb_active = ch_mycol < ch_npcol && ch_myrow < ch_nprow && ch->dim_upd;
                if (ch_cb_active) {
                  auto I = ch->upd_to_parent(this);
                  auto pa = this;
                  auto ch_rowl2g_fixed = [&](int row ) -> int {
                    return indxl2g(row+1, DistM_t::default_MB, ch_myrow, 0, ch_nprow) - 1; };
                  auto ch_coll2g_fixed = [&](int col ) -> int {
                    return indxl2g(col+1, DistM_t::default_NB, ch_mycol, 0, ch_npcol) - 1; };

                  int ch_F22_lrows = scalapack::numroc(ch->dim_upd, DistM_t::default_MB, ch_myrow, 0, ch_nprow);
                  int ch_F22_lcols = scalapack::numroc(ch->dim_upd, DistM_t::default_NB, ch_mycol, 0, ch_npcol);

                  const auto lrows = ch_F22_lrows;
                  const auto lcols = ch_F22_lcols;
                  const auto pa_sep = pa->dim_sep;
                  const auto prows = pa->proc_rows;
                  const auto pcols = pa->proc_cols;
                  const auto B = DistM_t::default_MB;

                  // destination rank is:
                  //  ((r / B) % prows) + ((c / B) % pcols) * prows
                  //  = pr[r] + pc[c]
                  pa_container.resize(ch_F22_lrows+ch_F22_lcols);
                  auto & pr = pa_ptr.pr;
                  auto & pc = pa_ptr.pc;
                  auto & r_upd = pa_ptr.r_upd;
                  auto & c_upd = pa_ptr.c_upd;

                  pr = pa_container.data();
                  pc = pr + ch_F22_lrows;

                  for (r_upd=0; r_upd<lrows; r_upd++) {
                    auto t = I[ch_rowl2g_fixed(r_upd)];
                    if (t >= std::size_t(pa_sep)) break;
                    pr[r_upd] = (t / B) % prows;
                  }
                  for (int r=r_upd; r<lrows; r++){
                    pr[r] = ((I[ch_rowl2g_fixed(r)]-pa_sep) / B) % prows;
                  }

                  for (c_upd=0; c_upd<lcols; c_upd++) {
                    auto t = I[ch_coll2g_fixed(c_upd)];
                    if (t >= std::size_t(pa_sep)) break;
                    pc[c_upd] = ((t / B) % pcols) * prows;
                  }
                  for (int c=c_upd; c<lcols; c++){
                    pc[c] = (((I[ch_coll2g_fixed(c)]-pa_sep) / B) % pcols) * prows;
                  }
                }
              }
            }
            else if (auto ch = dynamic_cast<F_t*>(chbase)) {
#ifdef _PROXY_MPI_P2P_
              auto & row_offsets = ch_row_offsets[ch==this->lchild?0:1];
              auto & col_offsets = ch_col_offsets[ch==this->lchild?0:1];
              row_offsets.resize(1);
              col_offsets.resize(1);
              bool pa_cb_active = myrow < nprow && mycol < npcol && (this->dim_upd || this->dim_sep) ;
              if ( pa_cb_active ) {
                const auto ch_dim_upd = ch->dim_upd;
                const auto ch_upd = ch->upd;
                const auto pa_upd = this->upd;
                const auto pa_sep = this->sep_begin;

                for (int r=0, ur=0; r<F11_lrows; r++) {
                  auto fgr = rowl2g_fixed(r) + pa_sep;
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  row_offsets[ 0 ].push_back(r);
                }
                for (int r=0, ur=0; r<F22_lrows; r++) {
                  auto fgr = pa_upd[rowl2g_fixed(r)];
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  row_offsets[ 0 ].push_back(r);
                }
                for (int c=0, uc=0; c<F11_lcols; c++) {
                  auto fgc = coll2g_fixed(c) + pa_sep;
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  col_offsets[ 0 ].push_back(c);
                }
                for (int c=0, uc=0; c<F22_lcols; c++) {
                  auto fgc = pa_upd[coll2g_fixed(c)];
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  col_offsets[ 0 ].push_back(c);
                }
              }

#else
              auto & ch_container = ch_offsets_container[ch==this->lchild?0:1];
              auto & ch_ptr = ch_offsets_ptr[ch==this->lchild?0:1];
              bool pa_cb_active = myrow < nprow && mycol < npcol && (this->dim_upd || this->dim_sep) ;
              if ( pa_cb_active ) {
                const auto ch_dim_upd = ch->dim_upd;
                const auto ch_upd = ch->upd;
                const auto pa_upd = this->upd;
                const auto pa_sep = this->sep_begin;

                ch_container.resize(F11_lrows+F11_lcols+F22_lrows+F22_lcols);
                auto & r_1 = ch_ptr.r_1;
                auto & c_1 = ch_ptr.c_1;
                auto & r_2 = ch_ptr.r_2;
                auto & c_2 = ch_ptr.c_2;
                auto & r_max_1 = ch_ptr.r_max_1;
                auto & r_max_2 = ch_ptr.r_max_2;
                auto & c_max_1 = ch_ptr.c_max_1;
                auto & c_max_2 = ch_ptr.c_max_2;

                r_1 = ch_container.data();
                c_1 = r_1 + F11_lrows;
                r_2 = c_1 + F11_lcols;
                c_2 = r_2 + F22_lrows;
                r_max_1 = 0;
                r_max_2 = 0;
                c_max_1 = 0;
                c_max_2 = 0;

                for (int r=0, ur=0; r<F11_lrows; r++) {
                  auto fgr = rowl2g_fixed(r) + pa_sep;
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  r_1[r_max_1++] = r;
                }
                for (int c=0, uc=0; c<F11_lcols; c++) {
                  auto fgc = coll2g_fixed(c) + pa_sep;
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  c_1[c_max_1++] = c;
                }
                for (int r=0, ur=0; r<F22_lrows; r++) {
                  auto fgr = pa_upd[rowl2g_fixed(r)];
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  r_2[r_max_2++] = r;
                }
                for (int c=0, uc=0; c<F22_lcols; c++) {
                  auto fgc = pa_upd[coll2g_fixed(c)];
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  c_2[c_max_2++] = c;
                }
              }
#endif

              auto & pa_container = pa_offsets_container[ch==this->lchild?0:1];
              auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];

              std::size_t u2s;
              const auto I = ch->upd_to_parent(this, u2s);
              const std::size_t du = ch->dim_upd;
              const std::size_t ds = this->dim_sep;
              const auto prows = this->proc_rows;
              const auto pcols = this->proc_cols;
              const auto B = DistM_t::default_MB;

              pa_container.resize(ch->dim_upd+ch->dim_upd);
              auto & pr = pa_ptr.pr;
              auto & pc = pa_ptr.pc;
              auto & r_upd = pa_ptr.r_upd;
              r_upd = u2s;

              pr = pa_container.data();
              pc = pr + ch->dim_upd;

              // destination rank is:
              //  ((r / B) % prows) + ((c / B) % pcols) * prows
              //  = pr[r] + pc[c]
              for (std::size_t i=0; i<u2s; i++) {
                auto Ii = I[i];
                pr[i] = (Ii / B) % prows;
                pc[i] = ((Ii / B) % pcols) * prows;
              }
              for (std::size_t i=u2s; i<du; i++) {
                auto Ii = I[i] - ds;
                pr[i] = (Ii / B) % prows;
                pc[i] = ((Ii / B) % pcols) * prows;
              }
            }
          }
        }
      }
#else
      if ( bactive) {
        //pre compute mapping arrays
        blacs_upcxx::team_desc tdesc(*this->active_front_team, this->proc_rows , this->proc_cols);

        int F11_lrows = scalapack::numroc(this->dim_sep, DistM_t::default_MB, tdesc.myrow, 0, tdesc.nprow);
        int F11_lcols = scalapack::numroc(this->dim_sep, DistM_t::default_NB, tdesc.mycol, 0, tdesc.npcol);
        int F22_lrows = scalapack::numroc(this->dim_upd, DistM_t::default_MB, tdesc.myrow, 0, tdesc.nprow);
        int F22_lcols = scalapack::numroc(this->dim_upd, DistM_t::default_NB, tdesc.mycol, 0, tdesc.npcol);
        auto rowl2g_fixed = [&tdesc](int row ) -> int {
          return indxl2g(row+1, DistM_t::default_MB, tdesc.myrow, 0, tdesc.nprow) - 1; };
        auto coll2g_fixed = [&tdesc](int col ) -> int {
          return indxl2g(col+1, DistM_t::default_NB, tdesc.mycol, 0, tdesc.npcol) - 1; };


        for (auto chbase : {this->lchild, this->rchild}) {
          if ( chbase != nullptr ) {
            if (auto ch = dynamic_cast<FMPI_t*>(chbase)) {
              auto & row_offsets = ch_row_offsets[ch==this->lchild?0:1];
              auto & col_offsets = ch_col_offsets[ch==this->lchild?0:1];
              row_offsets.resize(ch->proc_rows);
              col_offsets.resize(ch->proc_cols);
              bool pa_cb_active = tdesc.active() && (this->dim_upd || this->dim_sep) ;

              if ( pa_cb_active ) {
                const auto ch_dim_upd = ch->dim_upd;
                const auto ch_upd = ch->upd;
                const auto pa_upd = this->upd;
                const auto pa_sep = this->sep_begin;
                const auto prows = ch->proc_rows;
                const auto pcols = ch->proc_cols;
                const auto B = DistM_t::default_MB;

                for (int r=0, ur=0; r< F11_lrows; r++) {
                  auto fgr = rowl2g_fixed(r) + pa_sep;
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  row_offsets[ (ur / B) % prows ].push_back(r);
                }

                for (int r=0, ur=0; r< F22_lrows; r++) {
                  auto fgr = pa_upd[rowl2g_fixed(r)];
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  row_offsets[ (ur / B) % prows ].push_back(r);
                }

                for (int c=0, uc=0; c< F11_lcols; c++) {
                  auto fgc = coll2g_fixed(c) + pa_sep;
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  col_offsets[ (uc / B) % pcols ].push_back(c);
                }

                for (int c=0, uc=0; c< F22_lcols; c++) {
                  auto fgc = pa_upd[coll2g_fixed(c)];
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  col_offsets[ (uc / B) % pcols ].push_back(c);
                }
              }

              auto & pa_container = pa_offsets_container[ch==this->lchild?0:1];
              auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];
              if ( ch->active() ){
                blacs_upcxx::team_desc ch_tdesc(*ch->active_front_team, ch->proc_rows , ch->proc_cols);
                bool ch_cb_active = ch_tdesc.active() && ch->dim_upd;
                if (ch_cb_active) {
                  auto I = ch->upd_to_parent(this);
                  auto pa = this;
                  auto ch_rowl2g_fixed = [&ch_tdesc](int row ) -> int {
                    return indxl2g(row+1, DistM_t::default_MB, ch_tdesc.myrow, 0, ch_tdesc.nprow) - 1; };
                  auto ch_coll2g_fixed = [&ch_tdesc](int col ) -> int {
                    return indxl2g(col+1, DistM_t::default_NB, ch_tdesc.mycol, 0, ch_tdesc.npcol) - 1; };

                  int ch_F22_lrows = scalapack::numroc(ch->dim_upd, DistM_t::default_MB, ch_tdesc.myrow, 0, ch_tdesc.nprow);
                  int ch_F22_lcols = scalapack::numroc(ch->dim_upd, DistM_t::default_NB, ch_tdesc.mycol, 0, ch_tdesc.npcol);

                  const auto lrows = ch_F22_lrows;
                  const auto lcols = ch_F22_lcols;
                  const auto pa_sep = pa->dim_sep;
                  const auto prows = pa->proc_rows;
                  const auto pcols = pa->proc_cols;
                  const auto B = DistM_t::default_MB;

                  // destination rank is:
                  //  ((r / B) % prows) + ((c / B) % pcols) * prows
                  //  = pr[r] + pc[c]
                  pa_container.resize(ch_F22_lrows+ch_F22_lcols);
                  auto & pr = pa_ptr.pr;
                  auto & pc = pa_ptr.pc;
                  auto & r_upd = pa_ptr.r_upd;
                  auto & c_upd = pa_ptr.c_upd;

                  pr = pa_container.data();
                  pc = pr + ch_F22_lrows;

                  for (r_upd=0; r_upd<lrows; r_upd++) {
                    auto t = I[ch_rowl2g_fixed(r_upd)];
                    if (t >= std::size_t(pa_sep)) break;
                    pr[r_upd] = (t / B) % prows;
                  }
                  for (int r=r_upd; r<lrows; r++){
                    pr[r] = ((I[ch_rowl2g_fixed(r)]-pa_sep) / B) % prows;
                  }

                  for (c_upd=0; c_upd<lcols; c_upd++) {
                    auto t = I[ch_coll2g_fixed(c_upd)];
                    if (t >= std::size_t(pa_sep)) break;
                    pc[c_upd] = ((t / B) % pcols) * prows;
                  }
                  for (int c=c_upd; c<lcols; c++){
                    pc[c] = (((I[ch_coll2g_fixed(c)]-pa_sep) / B) % pcols) * prows;
                  }
                }
              }
            }
            else if (auto ch = dynamic_cast<F_t*>(chbase)) {
              auto & row_offsets = ch_row_offsets[ch==this->lchild?0:1];
              auto & col_offsets = ch_col_offsets[ch==this->lchild?0:1];
              row_offsets.resize(1);
              col_offsets.resize(1);
              bool pa_cb_active = tdesc.active() && (this->dim_upd || this->dim_sep) ;
              if ( pa_cb_active ) {
                const auto ch_dim_upd = ch->dim_upd;
                const auto ch_upd = ch->upd;
                const auto pa_upd = this->upd;
                const auto pa_sep = this->sep_begin;

                for (int r=0, ur=0; r<F11_lrows; r++) {
                  auto fgr = rowl2g_fixed(r) + pa_sep;
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  row_offsets[ 0 ].push_back(r);
                }
                for (int r=0, ur=0; r<F22_lrows; r++) {
                  auto fgr = pa_upd[rowl2g_fixed(r)];
                  while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
                  if (ur == ch_dim_upd) break;
                  if (ch_upd[ur] != fgr) continue;
                  row_offsets[ 0 ].push_back(r);
                }
                for (int c=0, uc=0; c<F11_lcols; c++) {
                  auto fgc = coll2g_fixed(c) + pa_sep;
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  col_offsets[ 0 ].push_back(c);
                }
                for (int c=0, uc=0; c<F22_lcols; c++) {
                  auto fgc = pa_upd[coll2g_fixed(c)];
                  while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
                  if (uc == ch_dim_upd) break;
                  if (ch_upd[uc] != fgc) continue;
                  col_offsets[ 0 ].push_back(c);
                }
              }

              auto & pa_container = pa_offsets_container[ch==this->lchild?0:1];
              auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];

              std::size_t u2s;
              const auto I = ch->upd_to_parent(this, u2s);
              const integer_t du = ch->dim_upd;
              const std::size_t ds = this->dim_sep;
              const auto prows = this->proc_rows;
              const auto pcols = this->proc_cols;
              const auto B = DistM_t::default_MB;

              pa_container.resize(2*du);
              auto & pr = pa_ptr.pr;
              auto & pc = pa_ptr.pc;
              auto & r_upd = pa_ptr.r_upd;
              r_upd = u2s;

              pr = pa_container.data();
              pc = pr + du;

              // destination rank is:
              //  ((r / B) % prows) + ((c / B) % pcols) * prows
              //  = pr[r] + pc[c]
              for (std::size_t i=0; i<u2s; i++) {
                auto Ii = I[i];
                pr[i] = (Ii / B) % prows;
                pc[i] = ((Ii / B) % pcols) * prows;
              }
              for (std::size_t i=u2s; i<(std::size_t)du; i++) {
                auto Ii = I[i] - ds;
                pr[i] = (Ii / B) % prows;
                pc[i] = ((Ii / B) % pcols) * prows;
              }
            }
          }
        }
      }

      if (this->active()){
        for (auto chbase : {this->lchild, this->rchild}) {
          bool left = chbase == this->lchild;
          if (auto ch = dynamic_cast<FMPI_t*>(chbase)) {
            int master = this->child_master(ch);
            const auto prows = ch->proc_rows;
            const auto pcols = ch->proc_cols;
            int NSource = prows*pcols;
            int num_recv = 0;
            for(int psource = master; psource<master+NSource;psource++){
              int local_psource = psource-master;
              auto & row_offsets = this->ch_row_offsets[left?0:1];
              auto & col_offsets = this->ch_col_offsets[left?0:1];
              auto & row_offset = row_offsets[local_psource % prows ]; 
              auto & col_offset = col_offsets[local_psource / prows ]; 

              size_t recv_size = row_offset.size()*col_offset.size();
              if( recv_size > 0 ) {
                num_recv++;
              }
            }
            extend_add_prom[ch==this->lchild?0:1].require_anonymous(num_recv);
          }
          else if (auto ch = dynamic_cast<F_t*>(chbase)) {
            auto & row_offsets = this->ch_row_offsets[left?0:1];
            auto & col_offsets = this->ch_col_offsets[left?0:1];
            auto & row_offset = row_offsets[0]; 
            auto & col_offset = col_offsets[0]; 
            int num_recv = 0;
            size_t recv_size = row_offset.size()*col_offset.size();
            if( recv_size > 0 ) {
              num_recv++;
            }
            extend_add_prom[ch==this->lchild?0:1].require_anonymous(num_recv);
          }
        }
      }
#endif
    }

  template<typename scalar_t,typename integer_t>
    FrontalMatrixMPI<scalar_t,integer_t>::~FrontalMatrixMPI() {
#ifdef _PROXY_MPI_BACKEND_
      if (ctxt != -1) Cblacs_gridexit(ctxt);
      if (ctxt_all != -1) Cblacs_gridexit(ctxt_all);
      mpi_free_comm(&front_comm);
#else
      if (active_front_team && active_front_team != &front_team ) {
        active_front_team->destroy();
        delete active_front_team;
      }

      front_team.destroy();
#endif
    }

  /** return the rank in front_comm where element r,c in matrix F is
    located */
  template<typename scalar_t,typename integer_t> inline int
    FrontalMatrixMPI<scalar_t,integer_t>::find_rank
    (integer_t r, integer_t c, const DistM_t& F) const {
      // the blacs grid is column major
      return ((r / F.MB()) % proc_rows) + ((c / F.NB()) % proc_cols)
        * proc_rows;
    }

  template<typename scalar_t,typename integer_t> inline int
    FrontalMatrixMPI<scalar_t,integer_t>::find_rank_fixed
    (integer_t r, integer_t c, const DistM_t& F) const {
      assert(F.fixed());
      return ((r / DistM_t::default_MB) % proc_rows)
        + ((c / DistM_t::default_NB) % proc_cols) * proc_rows;
    }

  /**
   * Check if the child needs to be visited not necessary when this rank
   * is not part of the processes assigned to the child.
   */
  template<typename scalar_t,typename integer_t> bool
    FrontalMatrixMPI<scalar_t,integer_t>::visit(const F_t* ch) const {
#ifdef _PROXY_MPI_BACKEND_
      if (!ch) return false;
      if (auto mpi_child = dynamic_cast<const FMPI_t*>(ch)) {
        if (mpi_child->front_comm == MPI_COMM_NULL)
          return false; // child is MPI
      } else if (mpi_rank(front_comm) != child_master(ch))
        return false; // child is sequential
      return true;
#else
      if (!ch) return false;
      if (auto mpi_child = dynamic_cast<const FMPI_t*>(ch)) {
        if (!bactive)
          return false; // child is MPI
      } else if ( front_team.rank_me() != child_master(ch))
        return false; // child is sequential
      return true;
#endif
    }

  template<typename scalar_t,typename integer_t> int
    FrontalMatrixMPI<scalar_t,integer_t>::child_master(const F_t* ch) const {
      int ch_master;
      if (auto mpi_ch = dynamic_cast<const FMPI_t*>(ch))
        ch_master = (ch == this->lchild) ? 0 : total_procs - mpi_ch->total_procs;
      else ch_master = (ch == this->lchild) ? 0 : total_procs - 1;
      return ch_master;
    }

#ifdef _PROXY_MPI_BACKEND_
  template<typename scalar_t,typename integer_t> long long
    FrontalMatrixMPI<scalar_t,integer_t>::dense_factor_nonzeros
    (int task_depth) const {
#ifdef _PROXY_MPI_BACKEND_
      long long nnz =
        (this->front_comm != MPI_COMM_NULL &&
         mpi_rank(this->front_comm) == 0) ?
        this->dim_blk*this->dim_blk-this->dim_upd*this->dim_upd : 0;
#else
      long long nnz =
        (
         this->front_team.rank_me() == 0) ?
        this->dim_blk*this->dim_blk-this->dim_upd*this->dim_upd : 0;
#endif
      if (visit(this->lchild))
        nnz += this->lchild->dense_factor_nonzeros(task_depth);
      if (visit(this->rchild))
        nnz += this->rchild->dense_factor_nonzeros(task_depth);
      return nnz;
    }
#endif

  template<typename scalar_t,typename integer_t> void
    FrontalMatrixMPI<scalar_t,integer_t>::extend_add() {
      if (!this->lchild && !this->rchild  ){
#ifndef _PROXY_MPI_BACKEND_
        auto fut_prom = extend_add_prom[0].finalize();
        fut_prom = upcxx::when_all(fut_prom,extend_add_prom[1].finalize());
        fut_prom.wait();
#endif
        return;
      }
#ifdef _PROXY_MPI_BACKEND_
      auto P = mpi_nprocs(this->front_comm);
#else
      auto P = this->proc_cols*this->proc_rows;
#endif

#ifdef _PROXY_MPI_BACKEND_


#ifdef _PROXY_MPI_P2P_
      int myrank = mpi_rank(this->front_comm);
      int tag = mpi_rank(this->front_comm);

      MPI_Datatype type = mpi_type<scalar_t>(); 
      std::array<std::vector<std::vector<scalar_t>>,2> rbufs;
      //prepost the receives
      int num_recv = 0;
      for (auto ch : {this->lchild, this->rchild}) {
        if ( ch != nullptr ) {
          bool left = ch == this->lchild;
          auto & rbuf = rbufs[left?0:1];

          int ltag = left?tag:tag+P; 
          int master = this->child_master(ch);
          if (auto ch_mpi = dynamic_cast<FMPI_t*>(ch)) {
            const auto prows = ch_mpi->proc_rows;
            const auto pcols = ch_mpi->proc_cols;
            int NSource = prows*pcols;
            rbuf.resize(P);
            for(int psource = master; psource<master+NSource;psource++){
              size_t recv_size = 0;
              int offset = (left?0:P)+psource;
              int local_psource = psource-master;
              MPI_Request & request = recv_requests[offset];

              auto & row_offsets = this->ch_row_offsets[left?0:1];
              auto & col_offsets = this->ch_col_offsets[left?0:1];
              auto & row_offset = row_offsets[local_psource % prows ]; 
              auto & col_offset = col_offsets[local_psource / prows ]; 

              recv_size = row_offset.size()*col_offset.size();
              rbuf[psource].resize(recv_size);
#ifdef _VERBOSE_
              logfile<<this->sep<<" receiving "<<ch->sep<<" from "<<psource<<" on tag "<<ltag<<" size "<<recv_size<<std::endl;
#endif
              if( recv_size > 0 ) {
                num_recv++;
                //post the recv
                MPI_Irecv(rbuf[psource].data(),recv_size,type,psource,ltag,this->front_comm,&request);
              }
            }
          } else if (auto ch_seq = dynamic_cast<F_t*>(ch)) {
            int psource = master;
            int NSource = 1;
            rbuf.resize(NSource);
            size_t recv_size = 0;
            int offset = (left?0:P)+psource;
            MPI_Request & request = recv_requests[offset];

            auto & row_offsets = this->ch_row_offsets[left?0:1];
            auto & col_offsets = this->ch_col_offsets[left?0:1];
            auto & row_offset = row_offsets[0]; 
            auto & col_offset = col_offsets[0]; 

            recv_size = row_offset.size()*col_offset.size();
            rbuf[0].resize(recv_size);
            if( recv_size > 0 ) {
              num_recv++;
#ifdef _VERBOSE_
              logfile<<this->sep<<" receiving SEQ "<<ch->sep<<" from "<<psource<<" on tag "<<ltag<<" size "<<recv_size<<std::endl;
#endif
              //post the recv
              MPI_Irecv(rbuf[0].data(),recv_size,type,psource,ltag,this->front_comm,&request);
            }
          }
        }
      }

      //pack the data
      std::array<std::vector<std::vector<scalar_t>>,2> sbufs;
      for (auto ch : {this->lchild, this->rchild}) {
        if ( ch != nullptr ) {
          if (!this->visit(ch)) continue;
          bool left = ch == this->lchild;
          auto & sbuf = sbufs[ch==this->lchild?0:1];
          sbuf.resize(P);
          if (auto ch_mpi = dynamic_cast<FMPI_t*>(ch)) {
            auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];
            ExtAdd::extend_add_copy_to_buffers_copy(ch_mpi->F22, 
                sbuf, this, pa_ptr.pr, pa_ptr.pc, pa_ptr.r_upd, pa_ptr.c_upd );
            //send the data
            for (int lp = 0; lp<P; lp++) {
              int p = (myrank+1+lp)%P; 
              int ltag = left?p:p+P; 
              int offset = (ch==this->lchild?0:P)+p;
              MPI_Request & request = send_requests[offset];
              //one has to send 0 bytes if processor is not active
              bool active = p < this->proc_cols*this->proc_rows;
              size_t send_size = active?sbuf[p].size():0;

              if( send_size > 0 ) {
#ifdef _VERBOSE_
                logfile<<this->sep<<" sending "<<ch->sep<<" to "<<p<<" on tag "<<ltag<<" size "<<send_size<<std::endl;
#endif
                MPI_Isend(sbuf[p].data(),send_size,type,p,ltag,this->front_comm,&request);
              }
            }

          } else if (auto ch_seq = dynamic_cast<F_t*>(ch)) {
            if (mpi_rank(this->front_comm) == this->child_master(ch)){
              auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];
              ExtAdd::extend_add_seq_copy_to_buffers_copy(ch_seq->F22, 
                  sbuf, pa_ptr.pr, pa_ptr.pc, pa_ptr.r_upd, this, ch_seq );
              //send the data
              for (int lp = 0; lp<P; lp++) {
                int p = (myrank+1+lp)%P; 
                int ltag = left?p:p+P; 
                int offset = (ch==this->lchild?0:P)+p;
                MPI_Request & request = send_requests[offset];
                bool active = p < this->proc_cols*this->proc_rows;
                size_t send_size = active?sbuf[p].size():0;
                if( send_size > 0 ) {
#ifdef _VERBOSE_
                  logfile<<this->sep<<" sending SEQ "<<ch->sep<<" to "<<p<<" on tag "<<ltag<<" size "<<send_size<<std::endl;
#endif
                  MPI_Isend(sbuf[p].data(),send_size,type,p,ltag,this->front_comm,&request);
                }
              }
            }
          }

        }
      }

      //implement a while loop with MPI_Waitany
      int recvd = 0;
      while(recvd<num_recv){
        MPI_Status status;
        int recv_idx = -1;
        MPI_Waitany(recv_requests.size(),recv_requests.data(),&recv_idx,&status);

        if (recv_idx != MPI_UNDEFINED){
          int psource = status.MPI_SOURCE;
          int recv_size = 0;
          MPI_Get_count( &status, type, &recv_size );
          bool left = recv_idx<P;

          auto & row_offsets = this->ch_row_offsets[left?0:1];
          auto & col_offsets = this->ch_col_offsets[left?0:1];
          auto chbase = left? this->lchild:this->rchild;
          int master = this->child_master(chbase);
#ifdef _VERBOSE_
          logfile<<this->sep<<" received "<<chbase->sep<<" from "<<psource<<" on tag "<<status.MPI_TAG<<" size "<<recv_size<<std::endl;
#endif
          if(auto ch = dynamic_cast<FMPI_t*>(chbase)){
            auto & rbuf = rbufs[left?0:1][psource];
            const auto prows = ch->proc_rows;
            int local_psource = psource-master;
            auto & row_offset = row_offsets[local_psource % prows ]; 
            auto & col_offset = col_offsets[local_psource / prows ]; 

            auto it = &rbuf[0];
            for( std::size_t offc = 0; offc<col_offset.size(); offc++){
              for( std::size_t offr = 0; offr<row_offset.size(); offr++){
                int r = row_offset[offr];
                int c = col_offset[offc];

                auto & val = *it;
                if ( r < F11.lrows() ) { 
                  if ( c < F11.lcols() ) //F11
                    F11(r,c) += val;
                  else //F12
                    F12(r,c) += val;
                }
                else {
                  if ( c < F11.lcols() ) // F21
                    F21(r,c) += val;
                  else //F22
                    F22(r,c) += val;
                }

                it++;
              }
            }
          } else if (auto ch_seq = dynamic_cast<F_t*>(ch)) {
            auto & rbuf = rbufs[left?0:1][0];
            assert(psource == master);
            auto & row_offset = row_offsets[ 0 ]; 
            auto & col_offset = col_offsets[ 0 ]; 

            auto it = &rbuf[0];
            for( std::size_t offc = 0; offc<col_offset.size(); offc++){
              for( std::size_t offr = 0; offr<row_offset.size(); offr++){
                int r = row_offset[offr];
                int c = col_offset[offc];

                auto & val = *it;
                if ( r < F11.lrows() ) { 
                  if ( c < F11.lcols() ) //F11
                    F11(r,c) += val;
                  else //F12
                    F12(r,c) += val;
                }
                else {
                  if ( c < F11.lcols() ) // F21
                    F21(r,c) += val;
                  else //F22
                    F22(r,c) += val;
                }

                it++;
              }
            }
          }
          recvd++;
        }
        else{
          abort();
        }

      }

      //sync on the send requests as well
      MPI_Waitall(send_requests.size(),send_requests.data(),send_statuses.data());
#else
      std::vector<std::vector<scalar_t>> sbuf(P);
      for (auto ch : {this->lchild, this->rchild}) {
        if ( ch != nullptr ) {
          if (!this->visit(ch)) continue;
          if (auto ch_mpi = dynamic_cast<FMPI_t*>(ch)) {
            auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];
            ExtAdd::extend_add_copy_to_buffers_copy(ch_mpi->F22, 
                sbuf, this, pa_ptr.pr, pa_ptr.pc, pa_ptr.r_upd, pa_ptr.c_upd );

          } else if (auto ch_seq = dynamic_cast<F_t*>(ch)) {
            if (mpi_rank(this->front_comm) == this->child_master(ch)){
              auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];
              ExtAdd::extend_add_seq_copy_to_buffers_copy(ch_seq->F22, 
                  sbuf, pa_ptr.pr, pa_ptr.pc, pa_ptr.r_upd, this, ch_seq );
            }
          }   
        }
      }
      scalar_t *rbuf = nullptr, **pbuf = nullptr;
      size_t totrsize = 0;
      all_to_all_v(sbuf, rbuf, pbuf, totrsize, this->front_comm);

      if ( totrsize>0 ) {
        for (auto ch : {this->lchild, this->rchild}) {
          if ( ch != nullptr ) {
            if (auto ch_mpi = dynamic_cast<FMPI_t*>(ch)) {
              auto & ch_ptr = ch_offsets_ptr[ch==this->lchild?0:1];
              ExtAdd::extend_add_copy_from_buffers_copy(F11, F12, F21, F22, 
                  pbuf+this->child_master(ch), ch_ptr.upd_r_1, ch_ptr.upd_c_1, 
                  ch_ptr.upd_r_2, ch_ptr.upd_c_2, ch_ptr.r_1, ch_ptr.c_1, ch_ptr.r_2,
                  ch_ptr.c_2, ch_ptr.r_max_1, ch_ptr.r_max_2, ch_ptr.c_max_1, ch_ptr.c_max_2);

            } else if (auto ch_seq = dynamic_cast<F_t*>(ch)) {
              auto & ch_ptr = ch_offsets_ptr[ch==this->lchild?0:1];
              ExtAdd::extend_add_seq_copy_from_buffers_copy(F11, F12, F21, F22, 
                  ch_ptr.r_1, ch_ptr.c_1, ch_ptr.r_2, ch_ptr.c_2, 
                  ch_ptr.r_max_1, ch_ptr.r_max_2, ch_ptr.c_max_1, ch_ptr.c_max_2,pbuf[this->child_master(ch_seq)]);
            }
          } 
        }
      }
      delete[] pbuf;
      delete[] rbuf;
#endif

#else
      const auto prows = this->proc_rows;
      const auto pcols = this->proc_cols;

      int myrank = front_team.rank_me();
      Dist_FMPI_t dFM_pointers(front_team,this);
      std::array<std::vector<std::vector<scalar_t>>,2> sbufs;
      upcxx::future<> fut = upcxx::make_future();
      for (auto ch : {this->lchild, this->rchild}) {
        if ( ch != nullptr ) {
          if (!this->visit(ch)) continue;
          if (auto ch_mpi = dynamic_cast<FMPI_t*>(ch)) {
            if ( !ch_mpi->active() ) continue;
            auto & sbuf = sbufs[ch==this->lchild?0:1];
            sbuf.resize(P);
            auto & pa_container = pa_offsets_container[ch==this->lchild?0:1];
            auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];
            ExtAdd::extend_add_copy_to_buffers_copy(ch_mpi->F22, 
                sbuf, this, pa_ptr.pr, pa_ptr.pc, pa_ptr.r_upd, pa_ptr.c_upd );
            if (pa_container.size()>0) {
                //issue rpcs
                for (int lp=0; lp<P;lp++) {
                  int p = (myrank+1+lp)%P; 
                  assert(P<=front_team.rank_n());
                  if ( sbuf[p].size()>0 ) {
                    fut = upcxx::when_all(fut, upcxx::rpc( front_team[p], []( int psource, upcxx::view<scalar_t> values, Dist_FMPI_t & aFM, bool left){
                          FMPI_t * pa = *aFM; 
                          auto & F11 = pa->F11;
                          auto & F12 = pa->F12;
                          auto & F21 = pa->F21;
                          auto & F22 = pa->F22;

                          auto & prom = pa->extend_add_prom[left?0:1];
                          prom.fulfill_anonymous(1);
                          if (!(F11.active() || F22.active())) return;

                          FMPI_t * ch = left ? (FMPI_t*)pa->lchild : (FMPI_t*)pa->rchild;

                          const auto prows = ch->proc_rows;
                          const auto pcols = ch->proc_cols;

                          auto & row_offsets = pa->ch_row_offsets[left?0:1];
                          auto & col_offsets = pa->ch_col_offsets[left?0:1];
                          int idx = 0; 
                          auto & row_offset = row_offsets[psource % prows ]; 
                          auto & col_offset = col_offsets[psource / prows ]; 

                          auto it = values.begin();
                          for( std::size_t offc = 0; offc<col_offset.size(); offc++){
                            for( std::size_t offr = 0; offr<row_offset.size(); offr++){
                              int r = row_offset[offr];
                              int c = col_offset[offc];

                              auto & val = *it;
                              if ( r < F11.lrows() ) { 
                                if ( c < F11.lcols() ) //F11
                                  F11(r,c) += val;
                                else //F12
                                  F12(r,c) += val;
                              }
                              else {
                                if ( c < F11.lcols() ) // F21
                                  F21(r,c) += val;
                                else //F22
                                  F22(r,c) += val;
                              }

                              it++;
                            }
                          }

                    }  ,ch_mpi->active_front_team->rank_me(),upcxx::make_view( sbuf[p].begin(), sbuf[p].end() ), dFM_pointers, ch == this->lchild ));
                  }
                }
              }
          }
          else if (auto ch_seq = dynamic_cast<F_t*>(ch)) {
            if ( !bactive ) continue;
            if (this->front_team.rank_me() == this->child_master(ch)){
              auto & sbuf = sbufs[ch==this->lchild?0:1];
              sbuf.resize(P);
              auto & pa_container = pa_offsets_container[ch==this->lchild?0:1];
              auto & pa_ptr = pa_offsets_ptr[ch==this->lchild?0:1];
              ExtAdd::extend_add_seq_copy_to_buffers_copy(ch_seq->F22, 
                  sbuf, pa_ptr.pr, pa_ptr.pc, pa_ptr.r_upd, this, ch_seq );

              if (pa_container.size()>0) {
                  //issue rpcs
                  for (int lp=0; lp<P; lp++) {
                    int p = (myrank+1+lp)%P; 
                    assert(P<=front_team.rank_n());
                    if ( sbuf[p].size()>0 ) {
                      fut = upcxx::when_all(fut, upcxx::rpc( front_team[p], []( upcxx::view<scalar_t> values, Dist_FMPI_t & aFM, bool left){
                            FMPI_t * pa = *aFM; 

                            auto & F11 = pa->F11;
                            auto & F12 = pa->F12;
                            auto & F21 = pa->F21;
                            auto & F22 = pa->F22;

                            auto & prom = pa->extend_add_prom[left?0:1];
                            prom.fulfill_anonymous(1);
                            if (!(F11.active() || F22.active())) return;

                            FMPI_t * ch = left ? (FMPI_t*)pa->lchild : (FMPI_t*)pa->rchild;

                            const auto prows = ch->proc_rows;
                            const auto pcols = ch->proc_cols;

                            auto & row_offsets = pa->ch_row_offsets[left?0:1];
                            auto & col_offsets = pa->ch_col_offsets[left?0:1];
                            auto & row_offset = row_offsets[ 0 ]; 
                            auto & col_offset = col_offsets[ 0 ]; 
                            int idx = 0; 

                            auto it = values.begin();
                            for( std::size_t offc = 0; offc<col_offset.size(); offc++){
                              for( std::size_t offr = 0; offr<row_offset.size(); offr++){
                                int r = row_offset[offr];
                                int c = col_offset[offc];

                                auto & val = *it;
                                if ( r < F11.lrows() ) { 
                                  if ( c < F11.lcols() ) //F11
                                    F11(r,c) += val;
                                  else //F12
                                    F12(r,c) += val;
                                }
                                else {
                                  if ( c < F11.lcols() ) // F21
                                    F21(r,c) += val;
                                  else //F22
                                    F22(r,c) += val;
                                }

                                it++;
                              }
                            }
                      }  ,upcxx::make_view( sbuf[p].begin(), sbuf[p].end() ), dFM_pointers, ch == this->lchild ));
                    }
                  }
                }

            }
          }
        }  
      }

      fut = upcxx::when_all(fut,extend_add_prom[0].finalize());
      fut = upcxx::when_all(fut,extend_add_prom[1].finalize());
      fut.wait();
#endif
    }



  template<typename scalar_t,typename integer_t> void
    FrontalMatrixMPI<scalar_t,integer_t>::partial_factorization() {
#ifdef WITH_BARRIER
#ifdef _PROXY_MPI_BACKEND_
      MPI_Barrier(this->front_comm);
#else
      upcxx::barrier(front_team);
#endif
#endif

      if (this->dim_sep && F11.active()) {
        if (this->dim_upd) {
        }
      }
    }

  template<typename scalar_t,typename integer_t> void
    FrontalMatrixMPI<scalar_t,integer_t>::multifrontal_factorization
    (int etree_level, int task_depth) {
      if (this->visit(this->lchild)){
        if (auto ch = dynamic_cast<FMPI_t*>(this->lchild)) {
          this->lchild->multifrontal_factorization( etree_level+1, task_depth);
        }
#ifndef _PROXY_MPI_BACKEND_
        else if (front_team.rank_me() == this->child_master(this->lchild))
#else
        else if (mpi_rank(front_comm) == this->child_master(this->lchild))
#endif
        {
          this->lchild->multifrontal_factorization( etree_level+1, task_depth);
        }
      }
      if (this->visit(this->rchild)){
        if (auto ch = dynamic_cast<FMPI_t*>(this->rchild)) {
          this->rchild->multifrontal_factorization( etree_level+1, task_depth);
        }
#ifndef _PROXY_MPI_BACKEND_
        else if (front_team.rank_me() == this->child_master(this->rchild))
#else
        else if (mpi_rank(front_comm) == this->child_master(this->rchild))
#endif
        {
          this->rchild->multifrontal_factorization( etree_level+1, task_depth);
        }
      }

      build_front();
      if (this->lchild) this->lchild->release_work_memory();
      if (this->rchild) this->rchild->release_work_memory();
      partial_factorization();
    }


  template<typename scalar_t,typename integer_t> void
    FrontalMatrixMPI<scalar_t,integer_t>::release_work_memory() {
      F22.clear(); // remove the update block
    }

#ifdef _PROXY_MPI_BACKEND_
  template<typename scalar_t,typename integer_t> void
    FrontalMatrixMPI<scalar_t,integer_t>::build_front() {
      if (this->dim_sep) {
        F11 = DistM_t(this->ctxt, this->dim_sep, this->dim_sep);
        if (this->dim_upd) {
          F12 = DistM_t(this->ctxt, this->dim_sep, this->dim_upd);
          F21 = DistM_t(this->ctxt, this->dim_upd, this->dim_sep);
        }
      }
      if (this->dim_upd) {
        F22 = DistM_t(this->ctxt, this->dim_upd, this->dim_upd);
      }
      extend_add();
    }
#else
  template<typename scalar_t,typename integer_t> void
    FrontalMatrixMPI<scalar_t,integer_t>::build_front() {
      if ( active() ) {
        blacs_upcxx::team_desc tdesc(*active_front_team, proc_rows , proc_cols);
        if (this->dim_sep && (F11.lcols()==0 || F11.lrows()==0) )
          F11 = DistM_t(&tdesc, this->dim_sep, this->dim_sep);
        if (this->dim_upd && this->dim_sep && (F12.lcols()==0 || F12.lrows()==0) )
          F12 = DistM_t(&tdesc, this->dim_sep, this->dim_upd);
        if (this->dim_upd && this->dim_sep && (F21.lcols()==0 || F21.lrows()==0) )
          F21 = DistM_t(&tdesc, this->dim_upd, this->dim_sep);
        if (this->dim_upd && (F22.lcols()==0 || F22.lrows()==0)) {
          F22 = DistM_t(&tdesc, this->dim_upd, this->dim_upd);
        }
      }
      extend_add();
    }
#endif

  template<typename scalar_t,typename integer_t> void
    FrontalMatrixMPI<scalar_t,integer_t>::print_fronts () {
      std::stringstream sstr;
      sstr<<this->sep<<" ";
      sstr<<this->sep_begin<<" ";
      sstr<<this->sep_end<<" ";
      sstr<<this->dim_upd<<" [ "; for(int i = 0; i < this->dim_upd; i++) { sstr<< this->upd[i]<<" ";} sstr<<"]"<<std::endl;
      sstr<<this->dim_upd<<" F11 [ "; for(int i = 0; i < this->F11.lrows(); i++) { for(int j = 0; j < this->F11.lcols(); j++) { sstr<< this->F11(i,j)<<" ";} sstr<<std::endl;} sstr<<"]"<<std::endl;
      sstr<<this->dim_upd<<" F12 [ "; for(int i = 0; i < this->F12.lrows(); i++) { for(int j = 0; j < this->F12.lcols(); j++) { sstr<< this->F12(i,j)<<" ";} sstr<<std::endl;} sstr<<"]"<<std::endl;
      sstr<<this->dim_upd<<" F21 [ "; for(int i = 0; i < this->F21.lrows(); i++) { for(int j = 0; j < this->F21.lcols(); j++) { sstr<< this->F21(i,j)<<" ";} sstr<<std::endl;} sstr<<"]"<<std::endl;
      sstr<<this->dim_upd<<" F22 [ "; for(int i = 0; i < this->F22.lrows(); i++) { for(int j = 0; j < this->F22.lcols(); j++) { sstr<< this->F22(i,j)<<" ";} sstr<<std::endl;} sstr<<"]"<<std::endl;

      logfile<<sstr.str();

      if ( this->lchild )
        ((strumpack::FrontalMatrixMPI<double,int> *)this->lchild)->print_fronts();
      if ( this->rchild )
        ((strumpack::FrontalMatrixMPI<double,int> *)this->rchild)->print_fronts();
    }


} // end namespace strumpack

#endif //FRONTAL_MATRIX_MPI_HPP
