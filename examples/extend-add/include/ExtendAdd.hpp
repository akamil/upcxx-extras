/*
 * Portions of the code in this file are adapted from STRUMPACK, see strumpack/license.txt
 *
 */
#ifndef EXTEND_ADD_HPP
#define EXTEND_ADD_HPP

#include "DistributedMatrix.hpp"
#include "DenseMatrix.hpp"

namespace strumpack {
  template<typename scalar_t,typename integer_t> class FrontalMatrix;
  template<typename scalar_t,typename integer_t> class FrontalMatrixMPI;

  template<typename scalar_t,typename integer_t> class ExtendAdd {
    using DistM_t = DistributedMatrix<scalar_t>;
    using DM_t = DenseMatrix<scalar_t>;
    using FMPI_t = FrontalMatrixMPI<scalar_t,integer_t>;

  public:
    static void extend_add_copy_to_buffers_copy
    (const DistM_t& CB,
     std::vector<std::vector<scalar_t>>& sbuf,
     const FrontalMatrixMPI<scalar_t,integer_t>* pa,
     const int * pr,
     const int * pc,
     const int r_upd,
     const int c_upd 
     ) {
      if (!CB.active()) return;
      assert(CB.fixed());
      const auto lrows = CB.lrows();
      const auto lcols = CB.lcols();
      { // reserve space for the send buffers
        std::vector<std::size_t> cnt(sbuf.size());
        for (int c=0; c<lcols; c++)
          for (int r=0; r<lrows; r++)
            cnt[pr[r]+pc[c]]++;
        for (std::size_t p=0; p<sbuf.size(); p++)
          sbuf[p].reserve(sbuf[p].size()+cnt[p]);
      }
      for (int c=0; c<c_upd; c++) {// F11 & F21
        for (int r=0; r<r_upd; r++)
          sbuf[pr[r]+pc[c]].push_back(CB(r,c));
        for (int r=r_upd; r<lrows; r++)
          sbuf[pr[r]+pc[c]].push_back(CB(r,c));
      }
      for (int c=c_upd; c<lcols; c++) {// F12 & F22
        for (int r=0; r<r_upd; r++)
          sbuf[pr[r]+pc[c]].push_back(CB(r,c));
        for (int r=r_upd; r<lrows; r++)
          sbuf[pr[r]+pc[c]].push_back(CB(r,c));
      }
    }



    static void extend_add_copy_to_buffers
    (const DistM_t& CB, const DistM_t& F11, const DistM_t& F12,
     const DistM_t& F21, const DistM_t& F22,
     std::vector<std::vector<scalar_t>>& sbuf,
     const FrontalMatrixMPI<scalar_t,integer_t>* pa,
     const std::vector<std::size_t>& I) {
      if (!CB.active()) return;
      assert(CB.fixed());
      const auto lrows = CB.lrows();
      const auto lcols = CB.lcols();
      const auto pa_sep = pa->dim_sep;
      const auto prows = pa->proc_rows;
      const auto pcols = pa->proc_cols;
      const auto B = DistM_t::default_MB;
      // destination rank is:
      //  ((r / B) % prows) + ((c / B) % pcols) * prows
      //  = pr[r] + pc[c]
      auto pr = new int[CB.lrows()+CB.lcols()];
      auto pc = pr + CB.lrows();
      int r_upd, c_upd;
      for (r_upd=0; r_upd<lrows; r_upd++) {
        auto t = I[CB.rowl2g_fixed(r_upd)];
        if (t >= std::size_t(pa_sep)) break;
        pr[r_upd] = (t / B) % prows;
      }
      for (int r=r_upd; r<lrows; r++)
        pr[r] = ((I[CB.rowl2g_fixed(r)]-pa_sep) / B) % prows;

      for (c_upd=0; c_upd<lcols; c_upd++) {
        auto t = I[CB.coll2g_fixed(c_upd)];
        if (t >= std::size_t(pa_sep)) break;
        pc[c_upd] = ((t / B) % pcols) * prows;
      }
      for (int c=c_upd; c<lcols; c++)
        pc[c] = (((I[CB.coll2g_fixed(c)]-pa_sep) / B) % pcols) * prows;

      extend_add_copy_to_buffers_copy(CB, sbuf, pa, pr, pc, r_upd, c_upd );

      delete[] pr;
    }

    static void extend_add_seq_copy_to_buffers_copy
    (const  DM_t & CB, std::vector<std::vector<scalar_t>>& sbuf,
     const int * pr, const int * pc, std::size_t u2s,
     const FrontalMatrixMPI<scalar_t,integer_t>* pa,
     const FrontalMatrix<scalar_t,integer_t>* ch) {
      const std::size_t du = ch->dim_upd;
      { // reserve space for the send buffers
        std::vector<std::size_t> cnt(sbuf.size());
        for (std::size_t c=0; c<du; c++)
          for (std::size_t r=0; r<du; r++)
            cnt[pr[r]+pc[c]]++;
        for (std::size_t p=0; p<sbuf.size(); p++)
          sbuf[p].reserve(sbuf[p].size()+cnt[p]);
      }

      for (std::size_t c=0; c<u2s; c++){
        for (std::size_t r=0; r<u2s; r++) // F11
          sbuf[pr[r]+pc[c]].push_back(CB(r,c));
        for (std::size_t r=u2s; r<du; r++) // F21
          sbuf[pr[r]+pc[c]].push_back(CB(r,c));
      }

      for (std::size_t c=u2s; c<du; c++){
        for (std::size_t r=0; r<u2s; r++) // F12
          sbuf[pr[r]+pc[c]].push_back(CB(r,c));
        for (std::size_t r=u2s; r<du; r++) // F22
          sbuf[pr[r]+pc[c]].push_back(CB(r,c));
      }
    }




    static void extend_add_seq_copy_to_buffers
    (const  DM_t & CB, std::vector<std::vector<scalar_t>>& sbuf,
     const FrontalMatrixMPI<scalar_t,integer_t>* pa,
     const FrontalMatrix<scalar_t,integer_t>* ch) {
      std::size_t u2s;
      const auto I = ch->upd_to_parent(pa, u2s);
      const integer_t du = ch->dim_upd;
      const integer_t ds = pa->dim_sep;
      const auto pr = new int[CB.rows()+CB.cols()];
      const auto pc = pr + CB.rows();
      const auto prows = pa->proc_rows;
      const auto pcols = pa->proc_cols;
      const auto B = DistM_t::default_MB;
      // destination rank is:
      //  ((r / B) % prows) + ((c / B) % pcols) * prows
      //  = pr[r] + pc[c]
      for (std::size_t i=0; i<u2s; i++) {
        auto Ii = I[i];
        pr[i] = (Ii / B) % prows;
        pc[i] = ((Ii / B) % pcols) * prows;
      }
      for (std::size_t i=u2s; i<du; i++) {
        auto Ii = I[i] - ds;
        pr[i] = (Ii / B) % prows;
        pc[i] = ((Ii / B) % pcols) * prows;
      }

      extend_add_seq_copy_to_buffers_copy(CB, sbuf, pr, pc, u2s, pa, ch);

      delete[] pr;
    }



    static void extend_add_seq_copy_from_buffers_copy
    (DistM_t& F11, DistM_t& F12, DistM_t& F21, DistM_t& F22,
     const int * r_1, const int * c_1, const int * r_2, const int * c_2,
     integer_t r_max_1, integer_t r_max_2, integer_t c_max_1, integer_t c_max_2,
     scalar_t*& pbuf) {
      if (!(F11.active() || F22.active())) return;
      for (int c=0; c<c_max_1; c++) {
        for (int r=0; r<r_max_1; r++)
          F11(r_1[r],c_1[c]) += *(pbuf++);
        for (int r=0; r<r_max_2; r++)
          F21(r_2[r],c_1[c]) += *(pbuf++);
      }
      for (int c=0; c<c_max_2; c++) {
        for (int r=0; r<r_max_1; r++)
          F12(r_1[r],c_2[c]) += *(pbuf++);
        for (int r=0; r<r_max_2; r++)
          F22(r_2[r],c_2[c]) += *(pbuf++);
      }
    }


    static void extend_add_seq_copy_from_buffers
    (DistM_t& F11, DistM_t& F12, DistM_t& F21, DistM_t& F22,
     scalar_t*& pbuf, const FrontalMatrixMPI<scalar_t,integer_t>* pa,
     const FrontalMatrix<scalar_t,integer_t>* ch) {
      if (!(F11.active() || F22.active())) return;
      const auto ch_dim_upd = ch->dim_upd;
      const auto ch_upd = ch->upd;
      const auto pa_upd = pa->upd;
      const auto pa_sep = pa->sep_begin;
      auto r_1 =
        new int[F11.lrows()+F11.lcols()+F22.lrows()+F22.lcols()];
      auto c_1 = r_1 + F11.lrows();
      auto r_2 = c_1 + F11.lcols();
      auto c_2 = r_2 + F22.lrows();
      integer_t r_max_1 = 0, r_max_2 = 0;
      integer_t c_max_1 = 0, c_max_2 = 0;
      for (int r=0, ur=0; r<F11.lrows(); r++) {
        auto fgr = F11.rowl2g_fixed(r) + pa_sep;
        while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
        if (ur == ch_dim_upd) break;
        if (ch_upd[ur] != fgr) continue;
        r_1[r_max_1++] = r;
      }
      for (int c=0, uc=0; c<F11.lcols(); c++) {
        auto fgc = F11.coll2g_fixed(c) + pa_sep;
        while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
        if (uc == ch_dim_upd) break;
        if (ch_upd[uc] != fgc) continue;
        c_1[c_max_1++] = c;
      }
      for (int r=0, ur=0; r<F22.lrows(); r++) {
        auto fgr = pa_upd[F22.rowl2g_fixed(r)];
        while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
        if (ur == ch_dim_upd) break;
        if (ch_upd[ur] != fgr) continue;
        r_2[r_max_2++] = r;
      }
      for (int c=0, uc=0; c<F22.lcols(); c++) {
        auto fgc = pa_upd[F22.coll2g_fixed(c)];
        while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
        if (uc == ch_dim_upd) break;
        if (ch_upd[uc] != fgc) continue;
        c_2[c_max_2++] = c;
      }


    extend_add_seq_copy_from_buffers_copy
    (F11, F12, F21, F22, r_1, c_1, r_2, c_2,
     r_max_1, r_max_2, c_max_1, c_max_2, pbuf);

      delete[] r_1;
    }







    static void extend_add_copy_from_buffers_copy
    (DistM_t& F11, DistM_t& F12, DistM_t& F21, DistM_t& F22,
     scalar_t** pbuf, 
      const int * upd_r_1, const int * upd_c_1, const int * upd_r_2,
      const int * upd_c_2, const int * r_1, const int * c_1,
      const int * r_2, const int * c_2, const integer_t r_max_1,
      const integer_t r_max_2, const integer_t c_max_1, const integer_t c_max_2
     ) {
      if (!(F11.active() || F22.active())) return;
    
 
      for (int c=0; c<c_max_1; c++){
        for (int r=0; r<r_max_1; r++)
          F11(r_1[r],c_1[c]) += *(pbuf[upd_r_1[r]+upd_c_1[c]]++);
        for (int r=0; r<r_max_2; r++)
          F21(r_2[r],c_1[c]) += *(pbuf[upd_r_2[r]+upd_c_1[c]]++);
      }

      for (int c=0; c<c_max_2; c++){
        for (int r=0; r<r_max_1; r++)
          F12(r_1[r],c_2[c]) += *(pbuf[upd_r_1[r]+upd_c_2[c]]++);
        for (int r=0; r<r_max_2; r++)
          F22(r_2[r],c_2[c]) += *(pbuf[upd_r_2[r]+upd_c_2[c]]++);
      }
    }








    static void extend_add_copy_from_buffers
    (DistM_t& F11, DistM_t& F12, DistM_t& F21, DistM_t& F22,
     scalar_t** pbuf, const FrontalMatrixMPI<scalar_t,integer_t>* pa,
     const FrontalMatrixMPI<scalar_t,integer_t>* ch) {
      if (!(F11.active() || F22.active())) return;
      const auto ch_dim_upd = ch->dim_upd;
      const auto ch_upd = ch->upd;
      const auto pa_upd = pa->upd;
      const auto pa_sep = pa->sep_begin;
      const auto prows = ch->proc_rows;
      const auto pcols = ch->proc_cols;
      const auto B = DistM_t::default_MB;
      // source rank is
      //  ((r / B) % prows) + ((c / B) % pcols) * prows
      // where r,c is the coordinate in the F22 block of the child
      auto upd_r_1 =
        new int[2*F11.lrows()+2*F11.lcols()+
                2*F22.lrows()+2*F22.lcols()];
      auto upd_c_1 = upd_r_1 + F11.lrows();
      auto upd_r_2 = upd_c_1 + F11.lcols();
      auto upd_c_2 = upd_r_2 + F22.lrows();
      auto r_1 = upd_c_2 + F22.lcols();
      auto c_1 = r_1 + F11.lrows();
      auto r_2 = c_1 + F11.lcols();
      auto c_2 = r_2 + F22.lrows();
      integer_t r_max_1 = 0, r_max_2 = 0;
      integer_t c_max_1 = 0, c_max_2 = 0;
      for (int r=0, ur=0; r<F11.lrows(); r++) {
        auto fgr = F11.rowl2g_fixed(r) + pa_sep;
        while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
        if (ur == ch_dim_upd) break;
        if (ch_upd[ur] != fgr) continue;
        r_1[r_max_1] = r;
        upd_r_1[r_max_1++] = (ur / B) % prows;
      }
      for (int c=0, uc=0; c<F11.lcols(); c++) {
        auto fgc = F11.coll2g_fixed(c) + pa_sep;
        while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
        if (uc == ch_dim_upd) break;
        if (ch_upd[uc] != fgc) continue;
        c_1[c_max_1] = c;
        upd_c_1[c_max_1++] = ((uc / B) % pcols) * prows;
      }
      for (int r=0, ur=0; r<F22.lrows(); r++) {
        auto fgr = pa_upd[F22.rowl2g_fixed(r)];
        while (ur < ch_dim_upd && ch_upd[ur] < fgr) ur++;
        if (ur == ch_dim_upd) break;
        if (ch_upd[ur] != fgr) continue;
        r_2[r_max_2] = r;
        upd_r_2[r_max_2++] = (ur / B) % prows;
      }
      for (int c=0, uc=0; c<F22.lcols(); c++) {
        auto fgc = pa_upd[F22.coll2g_fixed(c)];
        while (uc < ch_dim_upd && ch_upd[uc] < fgc) uc++;
        if (uc == ch_dim_upd) break;
        if (ch_upd[uc] != fgc) continue;
        c_2[c_max_2] = c;
        upd_c_2[c_max_2++] = ((uc / B) % pcols) * prows;
      }

    extend_add_copy_from_buffers_copy(F11, F12, F21, F22, pbuf, upd_r_1, upd_c_1, upd_r_2,
      upd_c_2, r_1, c_1, r_2, c_2, r_max_1, r_max_2, c_max_1, c_max_2);

      delete[] upd_r_1;
    }

  };

} // end namespace strumpack

#endif // EXTEND_ADD_HPP
