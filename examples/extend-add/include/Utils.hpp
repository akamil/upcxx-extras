#ifndef UTILS_HPP
#define UTILS_HPP


#ifdef _PROXY_MPI_BACKEND_
#include "MPI_wrapper.hpp"
#else
#include <upcxx/upcxx.hpp>
#endif

#include <chrono>
#include <string>
#include <fstream>
extern std::ofstream logfile;

#include <thread>
#include <type_traits>

class scope_timer{
  public:
    using clock_t = std::chrono::steady_clock;

    using tp_t = std::chrono::time_point<clock_t>;
    using sec = std::chrono::duration<double>;
    std::string name;

    tp_t begin;
    tp_t end;
    bool stopped;

    scope_timer(std::string aname){
      static_assert(clock_t::is_steady, "scope_timer::clock_t must be steady.");
      name = aname;
      stopped = false;
      begin = clock_t::now();
    }

    inline void start(){
      begin = clock_t::now();
      stopped = false;
    }

    inline void stop(){
      end = clock_t::now();
      stopped = true;
    }
    ~scope_timer(){
      if (!stopped) end = clock_t::now();
      sec diff = std::chrono::duration_cast<sec>(end-begin);
      auto timer_value = diff.count();
      logfile<<"timer "<<name<<" local value: "<<diff.count()<<" s"<<std::endl;
#ifdef _PROXY_MPI_BACKEND_
      int mpirank = 0;
      MPI_Comm_rank(MPI_COMM_WORLD,&mpirank);
      if (mpirank==0) MPI_Reduce(MPI_IN_PLACE,&timer_value,1,MPI_DOUBLE,MPI_MAX,0, MPI_COMM_WORLD);
      else MPI_Reduce(&timer_value,&timer_value,1,MPI_DOUBLE,MPI_MAX,0, MPI_COMM_WORLD);
      if ( mpirank == 0 ) std::cout<<"timer "<<name<<" maximum value: "<<timer_value<<" s"<<std::endl;
#else
      timer_value = upcxx::reduce_one(timer_value,upcxx::op_fast_max,0).wait();
      if ( upcxx::rank_me() == 0 ) std::cout<<"timer "<<name<<" maximum value: "<<timer_value<<" s"<<std::endl;
#endif
    }
};


#endif //UTILS_HPP
