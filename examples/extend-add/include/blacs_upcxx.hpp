#ifndef BLACS_UPCXX
#define BLACS_UPCXX

#ifndef _PROXY_MPI_BACKEND_
#include <upcxx/upcxx.hpp>


#include <stdexcept>
#include <vector>
#include <limits>


  namespace blacs_upcxx{

    class team_desc{
      public:
      int nprow;
      int npcol;
      int myrow;
      int mycol;

      bool active() const {
        return myrow < nprow && mycol < npcol ;
      }

      upcxx::team & team;
      team_desc(
        upcxx::team & ateam,
        int anprow, int anpcol):team(ateam){
        int np = anprow * anpcol;
        nprow = anprow;
        npcol = anpcol;
        if ( ateam.rank_me()>= np) {
          myrow = -1;
          mycol = -1;
        }
        else{
          myrow = team.rank_me() % nprow;
          mycol = team.rank_me() / nprow;
        }
      }

    };

  }

#endif

#endif // BLACS_UPCXX
