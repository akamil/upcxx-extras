#ifndef UPCXX_SCALAPACK_HPP
#define UPCXX_SCALAPACK_HPP

#ifndef _PROXY_MPI_BACKEND_

  //scalapack replacement functions
namespace scalapack{
  int numroc(const int N, const int NB, const int IPROC, const int ISRCPROC, const int NPROCS){
    int MYDIST = (NPROCS+IPROC-ISRCPROC)%NPROCS;
    int NBLOCKS = N / NB;
    int retval = (NBLOCKS/NPROCS) * NB;
    int EXTRABLKS = NBLOCKS%NPROCS;
    if (MYDIST < EXTRABLKS) {
      retval += NB;
    }
    else if (MYDIST == EXTRABLKS) {
      retval += N%NB;
    }
    return retval;
  }

  void descset( int * DESC, const int M, const int N, const int MB, const int NB, const int IRSRC, const int ICSRC, const int ICTXT, const int LLD){
    DESC[0] = 1; //BLOCK_CYCLIC_2D
    DESC[1] = ICTXT;
    DESC[2] = M;
    DESC[3] = N;
    DESC[4] = MB;
    DESC[5] = NB;
    DESC[6] = IRSRC;
    DESC[7] = ICSRC;
    DESC[8] = LLD;
  } 

  int descinit( int * DESC, const int M, const int N, const int MB, const int NB, const int IRSRC, const int ICSRC, const blacs_upcxx::team_desc & tdesc, const int LLD){
    int INFO;
    int NPROW, NPCOL, MYROW, MYCOL;
    NPROW = tdesc.nprow;
    NPCOL = tdesc.npcol;
    MYROW = tdesc.myrow;
    MYCOL = tdesc.mycol;

    INFO = 0;
    if ( M < 0 ) {
      INFO = -2;
    }
    else if ( N < 0 ) {
      INFO = -3;
    }
    else if ( MB < 1 ) {
      INFO = -4;
    }
    else if ( NB < 1 ) {
      INFO = -5;
    }
    else if ( IRSRC < 0  ||  IRSRC >= NPROW ) {
      INFO = -6;
    }
    else if ( ICSRC < 0  ||  ICSRC >= NPCOL ) {
      INFO = -7;
    }
    else if ( NPROW == -1 ) {
      INFO = -8;
    }
    else if ( LLD < std::max( 1, numroc( M, MB, MYROW, IRSRC, NPROW ) ) ) {
      INFO = -9;
    }

    DESC[ 0 ] = 1;//BLOCK_CYCLIC_2D;
    DESC[ 1 ] = 0;//ICTXT;
    DESC[ 2 ] = std::max( 0, M );
    DESC[ 3 ] = std::max( 0, N );
    DESC[ 4 ] = std::max( 1, MB );
    DESC[ 5 ] = std::max( 1, NB );
    DESC[ 6 ] = std::max( 0, std::min( IRSRC, NPROW-1 ) );
    DESC[ 7 ] = std::max( 0, std::min( ICSRC, NPCOL-1 ) );
    DESC[ 8 ] = std::max( LLD, std::min( 1, numroc( DESC[ 2 ], DESC[ 4 ], MYROW, DESC[ 6 ], NPROW )));


    return INFO;
  }
}

#endif
#endif //UPCXX_SCALAPACK_HPP

