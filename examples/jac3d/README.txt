This directory contains an example UPC++/CUDA implementation of a Jacobi solver
for a three-dimensional system. The Jacobi method is a classical example of a
stencil computation, in this case a 3D stencil. As such, the bulk of the
communication performed in this example application occurs in the exchange of
halo regions.

This implementation depends on:
 
  - NVIDIA GPUs with compute capability 3.0 or higher
  - A working CUDA implementation (tested with CUDA 9.0 or later)
  - A working UPC++ v1.0 implementation (2019.3.0 or later),
    installed with CUDA support (see install docs)

An example Makefile is provided in this directory that illustrates the build
process. By default, it will use the UPCXX_INSTALL and CUDA_HOME environment
variables to locate the appropriate compilers under your UPC++ and CUDA
installations. If these environment variables are not set, it will assume that
the upcxx and nvcc compilers are in your PATH.

By default, this Makefile will compile CUDA kernels without specifying any GPU
code generation options, allowing nvcc's default to be used.  To specify a
single real or virtual architecture, set the NVCCARCH environment variable prior
to calling make. For instance, to generate code for the V100 GPUs on Summit:

    env NVCCARCH=sm_70 make

For more complex cases, one may instead set NVCCARCH_FLAGS:

    env NVCCARCH_FLAGS='-Wno-deprecated-gpu-targets -gencode arch=compute_35,code=compute_35' make


This Jacobi solver implementation takes the following command line arguments:

    -Nx	<integer>	domain size in the X dimension
    -Ny	<integer>	domain size in the Y dimension
    -Nz	<integer>	domain size in the Z dimension
    -N  <integer>   set same domain size in all 3 dimensions
    -i	<integer>	# iterations
    -px	<integer>	number of processors on x axis
    -py	<integer>	number of processors on y axis
    -pz	<integer>	number of processors on x axis

px * py * pz must equal the number of total ranks being used. Additionally, the
number of processors in each dimension/axis must evenly divide the domain size
in that dimension. If px/py/pz are not specified, the application will attempt
to generate a sane processor configuration for the user -- if this fails, an
error message is displayed and the application will abort.

For example, to run a 256 x 256 x 256 problem domain on a single Summit node
(using 4 GPUs) for 1000 iterations, you might use the following command:

jsrun -n 4 -a 1 -g 1 -c 1 ./jac3d -Nx 256 -Ny 256 -Nz 256 -i 1000 -px 2 -py 2 -pz 1

An equivalent command using the upcxx-run utility might look like:

upcxx-run -N 1 -n 4 ./jac3d -Nx 256 -Ny 256 -Nz 256 -i 1000 -px 2 -py 2 -pz 1

