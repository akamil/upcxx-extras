#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "config.hpp"

void printTOD(const char *mesg) {
    time_t tim = time(NULL);
    char *s = ctime(&tim);
    s[strlen(s)-1] = 0;
    printf("%s:  %s\n", mesg,s);
}

/****************************************************************
*								*
* returns the GFLOP rate					*
*								*
*****************************************************************/
double GFlops(const double time, const int Nx,const int Ny, const int Nz, const int iters)
{
#ifdef POISSON
    const double Ops = 7;
#else
    const double Ops = 6;
#endif
   double gflops = iters*(double)Nx*(double)Ny*(double)Nz*Ops/(1.0e9);
   if (time == 0.0) return 0.0;
   return (gflops/time);
}


